import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';

import { BackgroundGeolocation} from '@ionic-native/background-geolocation';
import { SQLite } from '@ionic-native/sqlite';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LoginPage } from '../pages/login/login';
import { NewleadPage } from '../pages/newlead/newlead';
import { CustomerPage } from '../pages/customer/customer';
import { BranchPage } from '../pages/branch/branch';
import { HelpPage } from '../pages/help/help';
import { ProductPage } from '../pages/product/product';
import { AddproductPage } from '../pages/addproduct/addproduct';
import { LeaddetailPage } from '../pages/leaddetail/leaddetail';
import { ProductdetailPage } from '../pages/productdetail/productdetail';
import { DvrPage } from '../pages/dvr/dvr';
//import { AdddvrPage } from '../pages/adddvr/adddvr';
import { DvrdetailPage } from '../pages/dvrdetail/dvrdetail';
import { ProfilePage } from '../pages/profile/profile';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { RestProvider } from '../providers/rest/rest';
import { StorageProvider } from '../providers/storage/storage';


import { ModalComponent } from '../components/modal/modal';
import { StatemodalComponent } from '../components/statemodal/statemodal';
import { EnddvrpopupComponent } from '../components/enddvrpopup/enddvrpopup';
import { MachinemodalComponent } from '../components/machinemodal/machinemodal';
import { DvrlocationComponent } from '../components/dvrlocation/dvrlocation';

import { LocationProvider } from '../providers/location/location';
import { LeadpopupComponent } from '../components/leadpopup/leadpopup';
import { CompititorComponent } from '../components/compititor/compititor';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
	LoginPage,
	NewleadPage,
	CustomerPage,
	BranchPage,
	HelpPage,
	ProductPage,
	AddproductPage,
	LeaddetailPage,
    ModalComponent,
	ProductdetailPage,
	ProfilePage,
	DvrPage,
    StatemodalComponent,
    EnddvrpopupComponent,
	DvrdetailPage,
    MachinemodalComponent,
    DvrlocationComponent,
    LeadpopupComponent,
    CompititorComponent
  ],
  imports: [
    BrowserModule,
   
	IonicModule.forRoot(MyApp),
	IonicStorageModule.forRoot({
      name: '__mydb',
         driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
	HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
	LoginPage,
	NewleadPage,
	CustomerPage,
	BranchPage,
	HelpPage,
	ProfilePage,
	ProductPage,
	AddproductPage,
	LeaddetailPage,
	ProductdetailPage,
	ModalComponent,
	StatemodalComponent,
	EnddvrpopupComponent,
	MachinemodalComponent,
	 DvrlocationComponent,
	 LeadpopupComponent,
	 CompititorComponent,
	DvrPage,
	
	DvrdetailPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RestProvider,
    StorageProvider,
	Geolocation,
	
	Diagnostic,
    BackgroundGeolocation,
	LocationProvider,
	SQLite
  ]
})
export class AppModule {}
