import { Component, ViewChild } from '@angular/core';
import { Nav, Platform ,LoadingController ,ToastController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { NewleadPage } from '../pages/newlead/newlead';
import { ProfilePage } from '../pages/profile/profile';
import { LoginPage } from '../pages/login/login';
import { CustomerPage } from '../pages/customer/customer';
import { BranchPage } from '../pages/branch/branch';
import { HelpPage } from '../pages/help/help';
import { ProductPage } from '../pages/product/product';
import { StorageProvider } from '../providers/storage/storage';
import { DvrPage } from '../pages/dvr/dvr';
import { RestProvider } from '../providers/rest/rest';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;
  errorMessage:any;
  loading:any;
  pages: Array<{title: string, component: any}>;

  constructor(public rest : RestProvider,public loadingCtrl: LoadingController,private toastCtrl: ToastController,public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,private storageService:StorageProvider,private sqlite: SQLite) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'New Lead', component: NewleadPage },
      { title: 'Leads', component: ListPage },
	  { title: 'Add Customer', component: CustomerPage },
      { title: 'Product Order', component: ProductPage },
	  { title: 'Help & Support', component: HelpPage },
      { title: 'Branch Office', component: BranchPage },
    { title: 'Manage Dvr', component: DvrPage },
    { title: 'Profile', component: ProfilePage },
	  
	  { title: 'Logout',component:'' }
    ];

   /* this.app.viewWillEnter.subscribe(
      () =>{

        this.storageService.getObject("tokan").then((val)=>{
          alert(val);
          
        }); 
      }
    ) */
	
	
	

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
     
      this.sqlite.create({
        name: 'salesdb.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql('CREATE TABLE IF NOT EXISTS locationdata(id INTEGER PRIMARY KEY, lat TEXT, long TEXT,time TEXT)', {})
        .then(res => console.log('Executed SQL'))
        .catch(e => console.log(e));
      
      });

    });
  }

  openPage(page) {
   if(page.title=='Logout'){
     this. showLoading();
    this.rest.logout().subscribe( res => {
       if(res.status ==200){ 
          this.storageService.setObject("tokan", '');
          this.nav.setRoot(LoginPage);
        }
        this.hideLoading();
      },
      error =>  {this.errorMessage = <any>error;
        this.hideLoading();
        this.showToast(this.errorMessage.message.error);
      });
	   
      
   }else{
    this.nav.push(page.component);
   }
  }


  showLoading(){
	  this.loading = this.loadingCtrl.create({
        content: 'Please wait...'
       });
	    this.loading.present();
  }
  
  hideLoading(){
	  this.loading.dismiss();
  }
  
  showToast(msg) {
	  let toast = this.toastCtrl.create({
		message: msg,
		duration: 2000,
		position: 'top'
	  });
      toast.present();
  }



}
