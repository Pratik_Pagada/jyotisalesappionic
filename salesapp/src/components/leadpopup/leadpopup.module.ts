import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { LeadpopupComponent } from './leadpopup';

@NgModule({
  declarations: [
    LeadpopupComponent,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    LeadpopupComponent
  ]
})
export class LeadpopupComponentModule {}
