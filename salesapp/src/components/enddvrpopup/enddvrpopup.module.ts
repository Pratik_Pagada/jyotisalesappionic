import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { EnddvrpopupComponent } from './enddvrpopup';

@NgModule({
  declarations: [
    EnddvrpopupComponent,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    EnddvrpopupComponent
  ]
})
export class EnddvrpopupComponentModule {}
