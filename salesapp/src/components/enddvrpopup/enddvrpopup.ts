import { Component } from '@angular/core';
import {  NavParams ,ViewController,LoadingController,ToastController } from 'ionic-angular';



@Component({
  selector: 'enddvrpopup',
  templateUrl: 'enddvrpopup.html'
})
export class EnddvrpopupComponent {

 
   data:any;
   enddvrform:any;
   transportlists:any;
   fuiellist:any;
   
   loading : any;

   constructor(public navParams: NavParams,private viewCtrl: ViewController,private toastCtrl: ToastController,public loadingCtrl: LoadingController) {
           
	  this.data = this.navParams.get('data');
	    this.enddvrform ={};
	   this.transportlists=[];
	   this.fuiellist =[];
	 this.transportlists = this.data.transport;
     this.fuiellist = this.data.fuiel;
	
  }
  
  
  enddvrsubmit(){
	  if(this.enddvrform.vehicletype){
		  
      if(this.enddvrform.vehicletype==91){
        if(this.enddvrform.fuieltype){
          this.viewCtrl.dismiss(this.enddvrform);
         }else{
          this.showToast("Please select fuel type")
         }
        } else if(this.enddvrform.vehicletype==92){
          if(this.enddvrform.priceltr){
            this.viewCtrl.dismiss(this.enddvrform);
           }else{
            this.showToast("Please enter amount")
           }
          }else{
          this.viewCtrl.dismiss(this.enddvrform);
        }        
	           
		 
	 }
	  
  }


  showToast(msg) {
	  let toast = this.toastCtrl.create({
		message: msg,
		duration: 2000,
		position: 'top'
	  });
      toast.present();
  }
  
  
  
   back(){
	   this.viewCtrl.dismiss();
   }

}
