import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { DvrlocationComponent } from './dvrlocation';

@NgModule({
  declarations: [
    DvrlocationComponent,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    DvrlocationComponent
  ]
})
export class DvrlocationComponentModule {}
