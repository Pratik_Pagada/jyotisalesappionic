import { Component } from '@angular/core';
import {  NavParams ,ViewController,LoadingController,ToastController ,ModalController} from 'ionic-angular';
import { ModalComponent } from '../../components/modal/modal';
import { LeadpopupComponent } from '../../components/leadpopup/leadpopup';
@Component({
  selector: 'dvrlocation',
  templateUrl: 'dvrlocation.html'
})
export class DvrlocationComponent {

   text: string;
   data:any;
   custmrlist:any;
   locanObj:any;
   dvrpinform:any;
   loading : any;
   visittypeList:any;
   leadlist:any;

   constructor(public navParams: NavParams,private viewCtrl: ViewController,private toastCtrl: ToastController,public loadingCtrl: LoadingController,public modalCtrl: ModalController) {
           
	  this.data = this.navParams.get('data');
	 
     this.custmrlist=[];
     this.leadlist = [];
	  this.visittypeList = [];
    this.custmrlist = this.data.custmrlist;
    this.leadlist = this.data.leadlist;
	  this.locanObj = this.data.locationobj;
	  this.visittypeList = this.data.visittypelist;
	  this.dvrpinform ={};
	  this.dvrpinform.customerid  = this.locanObj.iCustomerId;
    this.dvrpinform.iLocationId = this.locanObj.iLocationId;
    this.dvrpinform.lead = this.locanObj.vCustomerLocation;
    this.dvrpinform.customer = this.locanObj.company;
    this.dvrpinform.visittype = this.locanObj.iVisitType;
    this.dvrpinform.purpose = this.locanObj.vPurpose;
    this.dvrpinform.location = this.locanObj.vCustomerLocation;
  }
  
  
   selectGotram() {  
     
       let data = { "title": "Company", "data": this.custmrlist }
        let modal = this.modalCtrl.create(ModalComponent, data);
        modal.onDidDismiss(data => {
			
          if (data) {
          this.dvrpinform.customer = data.company;
		  this.dvrpinform.customerid = data.id;
		  
          }
        });
        modal.present();
}

selectGotram1() {  
  
    let data = { "title": "Company", "data": this.leadlist }
     let modal = this.modalCtrl.create(LeadpopupComponent, data);
     modal.onDidDismiss(data => {
   
       if (data) {
       this.dvrpinform.lead = data.leadNumber;
   this.dvrpinform.leadid = data.iLeadId;
   
       }
     });
     modal.present();
}
  
  
  pindvrsubmit(){
	 
		   if(this.dvrpinform.visittype){
        if(this.dvrpinform.visittype==159){
          if(this.dvrpinform.leadid){
                  this.viewCtrl.dismiss(this.dvrpinform);
          }else{
            this.showToast("Please select lead")
          }
        }else{
          this.viewCtrl.dismiss(this.dvrpinform);
        }
		  }else{
        this.showToast("Please select visit type")
      }
	 
	  
  }



  showToast(msg) {
	  let toast = this.toastCtrl.create({
		message: msg,
		duration: 2000,
		position: 'top'
	  });
      toast.present();
  }
  
  
   back(){
	   this.viewCtrl.dismiss();
   }


}
