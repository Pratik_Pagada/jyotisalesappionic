import { Component } from '@angular/core';
import {  NavParams ,ViewController,LoadingController } from 'ionic-angular';

@Component({
  selector: 'machinemodal',
  templateUrl: 'machinemodal.html'
})
export class MachinemodalComponent {

  text: string;
   data:any;
   companylists:any;
   auto_complete:any;
   loading : any;

    constructor(public navParams: NavParams,private viewCtrl: ViewController,public loadingCtrl: LoadingController) {
          
	    this.data = this.navParams.get('data');
	    this.companylists=[];
	    this.auto_complete =[];
	   this.companylists = this.data;
	  
       this.auto_complete = this.data;
	 
	  
  }
  
  
  
  
  
  
  
  
     getItems(ev) {
    
    let val = ev.target.value;
     
    if (val && val.trim() != '') {
      this.companylists = this.auto_complete.filter((item) => {
        return (item.product_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
    
  }
  clearItems(ev) {
    
    let val = ev.target.value;
   
    if (val && val.trim() != '') {
      this.companylists = this.auto_complete.filter((item) => {
        return (item.product_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
  selectedItem(item) {
    this.viewCtrl.dismiss(item);
  }
  
  back(){
	   this.viewCtrl.dismiss();
 }
  
  
   showLoading(){
	  this.loading = this.loadingCtrl.create({
        content: 'Please wait...'
       });
	    this.loading.present();
  }
  
  hideLoading(){
	  this.loading.dismiss();
  }
  
  
  
  
  
  
  
}
