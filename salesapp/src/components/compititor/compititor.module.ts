import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { CompititorComponent } from './compititor';

@NgModule({
  declarations: [
    CompititorComponent,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    CompititorComponent
  ]
})
export class CompititorComponentModule {}
