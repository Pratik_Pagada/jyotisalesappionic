import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,LoadingController,ToastController} from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { RestProvider } from '../../providers/rest/rest';



/**
 * Generated class for the HelpPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-help',
  templateUrl: 'help.html',
})
export class HelpPage {

  helptab:any;
  reportres:any;
  productList:string[];
  errorMessage:any;
  private reportform : FormGroup;
  private feedbackform : FormGroup;
  loading : any;
   
  constructor(public navCtrl: NavController, public navParams: NavParams,private formBuilder: FormBuilder, public rest : RestProvider,public loadingCtrl: LoadingController,private toastCtrl: ToastController) {
	  this.helptab = 'report';
	  
	  this.getproducts();
	  
	  this.reportform = this.formBuilder.group({
	    product: ['', Validators.required],
        name: ['', Validators.required],
		email: ['', Validators.required],
		comapny: ['', Validators.required],
		contact: ['', Validators.required],
        description: [''],
		address: [''],
		website: [''],
		message: [''],
     });
	 
	 this.feedbackform = this.formBuilder.group({
        name: ['', Validators.required],
		email: ['', Validators.required],
		comapny: ['', Validators.required],
		contact: ['', Validators.required],
        description: [''],
		address: [''],
		website: [''],
		message: [''],
    });
	 
	  this.rest.getTokan();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HelpPage');
  }
  
   back(){
	 this.navCtrl.pop(); 
  }
  
  getproducts() {
	  this.showLoading();
 
    this.rest.getproduct().subscribe(
	   productList => {this.productList = productList;
	    this.hideLoading();
	   },
	 error =>  {this.errorMessage = <any>error;
	    this.hideLoading();
	 });
  }
  
  
  
  ReportSubmit(){
	   this.showLoading();
	
	this.rest.reportsend(this.reportform.value).subscribe(
	   data => {this.reportres = data;
	    
		 this.hideLoading();
		 this.reportform.reset();
		 if(this.reportres.result =="success"){
			 this.showToast(this.reportres.message);
		 }
	   },
	 error =>  {this.errorMessage = <any>error;
	        this.hideLoading();
		    this.showToast(this.errorMessage);
	 });
  }
  
  
  FeedbackSubmit(){
	  this.showLoading();
	
	this.rest.reportsend(this.feedbackform.value).subscribe(
	   data => {this.reportres = data;
	  
	      this.hideLoading();
		  this.feedbackform.reset();
		  if(this.reportres.result =="success"){
			 this.showToast(this.reportres.message);
		 }
	   },
	 error =>  {this.errorMessage = <any>error;
	   this.hideLoading();
	   this.showToast(this.errorMessage);
	 });
	  
  }
  
  
  showLoading(){
	  this.loading = this.loadingCtrl.create({
        content: 'Please wait...'
       });
	    this.loading.present();
  }
  
  hideLoading(){
	  this.loading.dismiss();
  }
  
  showToast(msg) {
	  let toast = this.toastCtrl.create({
		message: msg,
		duration: 3000,
		position: 'top'
	  });
      toast.present();
  }
  

}
