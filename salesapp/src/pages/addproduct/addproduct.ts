import { Component ,ViewChild} from '@angular/core';
import { IonicPage, NavController,AlertController, NavParams ,LoadingController,Content,ToastController,ModalController} from 'ionic-angular';
import {Validators, FormBuilder, FormGroup,FormControl } from '@angular/forms';
import { RestProvider } from '../../providers/rest/rest';
import { MachinemodalComponent } from '../../components/machinemodal/machinemodal';
import { StorageProvider } from '../../providers/storage/storage';
import { LoginPage } from '../login/login';
import { ProductPage } from '../product/product';
import { CompititorComponent } from '../../components/compititor/compititor';

@IonicPage()
@Component({
  selector: 'page-addproduct',
  templateUrl: 'addproduct.html',
})
export class AddproductPage {
		@ViewChild(Content) content: Content;
		
	 private productform : FormGroup;
	 errorMessage:any;
	 loading : any;
     orderStatus:any;
	 comapnytypeList:any;
	 currencyList:any;
	 lostorderList:any;
	 modeofpaymentList:any;
	 orderstatusList:any;
	 probabilityList:any;
	 supporthdList:any;
	 allDropList:any;
	 controllerList:any;
	 modelList:any;
	 copititorList:any;
	 leadOBJ:any;
	 crntdate:any;
	 totalval:any;
	 modelslct:any;
	 modelid:any;
	  maxdate:any;
	  competitorLabel_1:any;
	  competitorLabel_2:any;
	  competitorId_1:any;
	  competitorId_2:any;
	 
  constructor(public alertCtrl: AlertController,private storageService:StorageProvider,public navCtrl: NavController, public navParams: NavParams,private formBuilder: FormBuilder, public rest : RestProvider,public loadingCtrl: LoadingController,private toastCtrl: ToastController,public modalCtrl: ModalController) {
	  
	  this.orderStatus ='';
	
	this.leadOBJ =   this.navParams.get('leadobj');
	  this.modelslct="";
	  
	    this.productform = this.formBuilder.group({
	    modelname: ['', Validators.required],
        controller: ['', Validators.required],
		machineval: ['', Validators.compose([Validators.required,Validators.pattern('[0-9]+$'), Validators.minLength(6)])],
		qty: ['', Validators.compose([Validators.required, Validators.pattern('[0-9]+$'),Validators.maxLength(3)])],
		totalval: ['', Validators.required],
        probability: ['', Validators.required],
		probabilityjyoti: ['', Validators.required],
		quotationno: ['', Validators.required],
		makingdate:['', Validators.required],
		ordersatus: ['', Validators.required],
		orderfinaldate:  ['', Validators.required],
		
		finalmachinqty: ['', Validators.required],
		finalval:  ['', Validators.compose([Validators.required, Validators.minLength(6)])],
		advanceamt:  ['', Validators.compose([Validators.required, Validators.minLength(5)])],
		collectiondate:  ['', Validators.required],
		
		paymentmode:  ['', Validators.required],
		currencytype:  ['', Validators.required],
		supporthd:[''],
		bankname:  ['', Validators.required],
		chequeno: ['', Validators.required],
		paymentterm:[''],
		reason:[''],
		lostordrdue: ['', Validators.required],
		compatitor1:[''],
		compatitor2:[''],
		othermodel: ['', Validators.required],
	//	photo:[''],
		comment:[''],
		dLiftingDate:['']
     });
	  
	  this.getdropdownlist();
	  this.getcontrollerlist();
	  this.getmodellist();
	  this.getcopititorList();
	  
	  let currentdate = new Date();
	  let crntDay =currentdate.getDate();
	  let crntMonth = currentdate.getMonth();
	  crntMonth = crntMonth+1;
	  let maxyear = currentdate.getFullYear();
	  maxyear = maxyear+5;
		
	if(crntDay<10 && crntMonth<10){this.maxdate = maxyear+"-0"+crntMonth+"-0"+crntDay;}
	else if (crntDay<10 && crntMonth>9){this.maxdate = maxyear+"-"+crntMonth+"-0"+crntDay;}
	else if (crntDay>9 && crntMonth<10){this.maxdate = maxyear+"-0"+crntMonth+"-"+crntDay;}
	else{this.maxdate = maxyear+"-"+crntMonth+"-"+crntDay; }


		
	   
	  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddproductPage');
	let today = new Date();
    let dd = today.getDate();
   let mm = today.getMonth()+1; 
   let yyyy = today.getFullYear();
   
   if(dd<10 && mm<10){this.crntdate = yyyy+"-0"+mm+"-0"+dd;}
   else if (dd<10 && mm>9){this.crntdate = yyyy+"-"+mm+"-0"+dd;}
   else if (dd>9 && mm<10){this.crntdate = yyyy+"-0"+mm+"-"+dd;}
   else{this.crntdate = yyyy+"-"+mm+"-"+dd; }
   
  
	
  }

    back(){
	 this.navCtrl.pop(); 
  }
  
  
  
   selectmodel() {  
   
        let data = { "title": "model", "data": this.modelList }
        let modal = this.modalCtrl.create(MachinemodalComponent, data);
          modal.onDidDismiss(data => {
             if (data) {
               this.modelslct = data.product_name;
		       this.modelid = data.id;
		     }
          });
        modal.present();
   }
  
   selectcompetitor_1() {  
	
	  let data = { "title": "Company", "data":  this.copititorList }
	   let modal = this.modalCtrl.create(CompititorComponent, data);
	   modal.onDidDismiss(data => {
		   
		 if (data) {
		  this.competitorLabel_1 = data.vCompetitorName;
		  this.competitorId_1 = data.iCompetitorListId;
		 
		 }
	   });
	   modal.present();
}

 selectcompetitor_2() {  
	
	  let data = { "title": "Company", "data":  this.copititorList }
	   let modal = this.modalCtrl.create(CompititorComponent, data);
	   modal.onDidDismiss(data => {
		   
		 if (data) {
		  this.competitorLabel_2 = data.vCompetitorName;
		  this.competitorId_2 = data.iCompetitorListId;
		 
		 }
	   });
	   modal.present();
}
  
  
  
  prdctTotal(mval,qnty){
	  if(mval){
		  if(qnty){
			  this.totalval = mval*qnty;
			
		  }
		  
	  }
	  
  }
  
  
  
   validateAllFormFields(formGroup: FormGroup) {         //{1}
 Object.keys(formGroup.controls).forEach(field => {  //{2}
   const control = formGroup.get(field);             //{3}
   if (control instanceof FormControl) {             //{4}
	 control.markAsTouched({ onlySelf: true });
   } else if (control instanceof FormGroup) {        //{5}
	 this.validateAllFormFields(control);            //{6}
   }
 });
}
  
  
  
  
  productformSubmit(){
	
	  if (this.productform.valid) {
	if(this.productform.value.qty>0){ 
		if(this.productform.value.advanceamt){
		
		if(Number(this.productform.value.advanceamt)<Number(this.productform.value.finalval)){  
		if(this.productform.value.finalmachinqty>0){
	
			let confirm = this.alertCtrl.create({
				title: '',
				message: 'Congratulation for your order confirmation.please tap yes to save.',
				buttons: [
				  {
					text: 'No',
					handler: () => {
					  console.log('Disagree clicked');
					}
				  },
				  {
					text: 'Yes',
					handler: () => {
	this.showLoading();
	  
	 this.rest.addProduct(this.productform.value,this.leadOBJ.iLeadId,this.totalval,this.modelid,this.competitorId_1,this.competitorId_2 ).subscribe(data => {
		 this.productform.reset();
		  this.hideLoading();
		   if(data.status ==200){ 
			 this.showToast(data.message.success);
			 this.navCtrl.push(ProductPage);

		   }
	   },
	   error =>  {this.errorMessage = <any>error;
	        this.hideLoading();
			 if(this.errorMessage.status == 401){ 
	         
		      this.storageService.setObject("tokan", '');
			  this.navCtrl.setRoot(LoginPage);
	         }
			this.showToast(this.errorMessage.message.error);
	 });

	}
}
]
});

confirm.present();
	
	}else{
		alert("Minimum Final Machine Qty is required 1");
	}
	}else{
		alert("Advance Amount is not grater than final value");
	}
   }else{
	this.showLoading();
	
   this.rest.addProduct(this.productform.value,this.leadOBJ.iLeadId,this.totalval,this.modelid,this.competitorId_1,this.competitorId_2  ).subscribe(data => {
	   this.productform.reset();
		this.hideLoading();
		 if(data.status ==200){ 
		   this.showToast(data.message.success);
		   this.navCtrl.push(ProductPage);

		 }
	 },
	 error =>  {this.errorMessage = <any>error;
		  this.hideLoading();
		   if(this.errorMessage.status == 401){ 
		   
			this.storageService.setObject("tokan", '');
			this.navCtrl.setRoot(LoginPage);
		   }
		  this.showToast(this.errorMessage.message.error);
   });

   }

	}else{
		alert("Minimum Quantity is required 1");
	}
	 
	  } else {
		this.validateAllFormFields(this.productform); //{7}
		this.content.scrollToTop();
	  }
	  
  }
  
  getdropdownlist(){
	   this.showLoading();
	 this.rest.getproductalldropdown().subscribe(data => {
		   this.allDropList = data;
	     
		   if(this.allDropList.status ==200){ 
			 for(let d =0; d<this.allDropList.data.length; d++){
				 if(this.allDropList.data[d].id==18){
					 this.currencyList = this.allDropList.data[d].optionvalues;
				 }else if(this.allDropList.data[d].id==16){
					 this.lostorderList = this.allDropList.data[d].optionvalues;
				 }else if(this.allDropList.data[d].id==17){
					 this.modeofpaymentList = this.allDropList.data[d].optionvalues;
				 }else if(this.allDropList.data[d].id==15){
					 this.orderstatusList = this.allDropList.data[d].optionvalues;
				 }else if(this.allDropList.data[d].id==13){
					 this.probabilityList = this.allDropList.data[d].optionvalues;
				 }else if(this.allDropList.data[d].id==14){
					 this.supporthdList = this.allDropList.data[d].optionvalues;
				 }
			 }
		   }
	   },
	 error =>  {this.errorMessage = <any>error;
	        
			this.showToast(this.errorMessage.message.error);
			
	 });
	 
	  
  }
  
  
  getcontrollerlist(){
	  
	 this.rest.getcontrollerdropdown().subscribe(data => {
		   this.allDropList = data;
	      if(this.allDropList.status ==200){ 
			 this.controllerList = this.allDropList.data;
		   }
	   },
	 error =>  {this.errorMessage = <any>error;
	     this.showToast(this.errorMessage.message.error);
	 });
 }
  
  
   getmodellist(){
	 
	 this.rest.getmodeldropdown().subscribe(data => {
		   this.allDropList = data;
	       if(this.allDropList.status ==200){ 
			 this.modelList = this.allDropList.data;
		   }
	   },
	 error =>  {this.errorMessage = <any>error;
	       
			this.showToast(this.errorMessage.message.error);
			
	 });
	 
	  
  }
  
  
  getcopititorList(){ 
  
     this.rest.getcopititorList().subscribe(data => {
		  this.hideLoading();
		   this.allDropList = data;
	       if(this.allDropList.status ==200){ 
			  this.copititorList =  this.allDropList.data;
		   }
	   },
	   error =>  {this.errorMessage = <any>error;
	        this.hideLoading();
			this.showToast(this.errorMessage.message.error);
	 });
	 
  }
  
  changeorderstatus(ordrsts){
	  if(ordrsts=="61"){
		   this.orderStatus="won";
		   this.productform.get('orderfinaldate').enable();
		   this.productform.get('finalmachinqty').enable();
		   this.productform.get('finalval').enable();
		   this.productform.get('advanceamt').enable();
		   this.productform.get('collectiondate').enable();
		   this.productform.get('paymentmode').enable();
		   this.productform.get('currencytype').enable();
		   this.productform.get('bankname').enable();
		   this.productform.get('chequeno').enable();
		   
		   this.productform.get('othermodel').disable();
		   this.productform.get('lostordrdue').disable();


		 
	  }else if(ordrsts=="62"){
		 this.orderStatus="lost";
		 this.productform.get('lostordrdue').enable();

		 this.productform.get('orderfinaldate').disable();
		 this.productform.get('finalmachinqty').disable();
		 this.productform.get('finalval').disable();
		 this.productform.get('advanceamt').disable();
		 this.productform.get('collectiondate').disable();
		 this.productform.get('paymentmode').disable();
		 this.productform.get('currencytype').disable();
		 this.productform.get('bankname').disable();
		 this.productform.get('chequeno').disable();
		 this.productform.get('othermodel').disable();

	  }else if(ordrsts=="64"){
		 this.orderStatus="other";
		 this.productform.get('othermodel').enable();

		 this.productform.get('lostordrdue').disable();
         this.productform.get('orderfinaldate').disable();
		this.productform.get('finalmachinqty').disable();
		this.productform.get('finalval').disable();
		this.productform.get('advanceamt').disable();
		this.productform.get('collectiondate').disable();
		this.productform.get('paymentmode').disable();
		this.productform.get('currencytype').disable();
		this.productform.get('bankname').disable();
		this.productform.get('chequeno').disable();

	  }else if(ordrsts=="65"){
		 this.orderStatus="drop";
		 this.productform.get('othermodel').disable();
		 
		this.productform.get('lostordrdue').disable();
		this.productform.get('orderfinaldate').disable();
		this.productform.get('finalmachinqty').disable();
		this.productform.get('finalval').disable();
		this.productform.get('advanceamt').disable();
		this.productform.get('collectiondate').disable();
		this.productform.get('paymentmode').disable();
		this.productform.get('currencytype').disable();
		this.productform.get('bankname').disable();
		this.productform.get('chequeno').disable();
	  }
	  else if(ordrsts=="63"){
		 this.orderStatus="pending";

		 this.productform.get('othermodel').disable();
		 
		this.productform.get('lostordrdue').disable();
		this.productform.get('orderfinaldate').disable();
		this.productform.get('finalmachinqty').disable();
		this.productform.get('finalval').disable();
		this.productform.get('advanceamt').disable();
		this.productform.get('collectiondate').disable();
		this.productform.get('paymentmode').disable();
		this.productform.get('currencytype').disable();
		this.productform.get('bankname').disable();
		this.productform.get('chequeno').disable();
	  }
	 
  }


  changepaymentmode(paymentmode){
	if(paymentmode == 77){
		this.productform.get('bankname').disable();
		this.productform.get('chequeno').disable();
	}else{
		this.productform.get('bankname').enable();
		this.productform.get('chequeno').enable();
	}

}
  
  
   showLoading(){
	  this.loading = this.loadingCtrl.create({
        content: 'Please wait...'
       });
	    this.loading.present();
  }
  
  hideLoading(){
	  this.loading.dismiss();
  }
  
  showToast(msg) {
	  let toast = this.toastCtrl.create({
		message: msg,
		duration: 2000,
		position: 'top'
	  });
      toast.present();
  }
  
  
  
}
