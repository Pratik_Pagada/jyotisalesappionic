import { Component } from '@angular/core';
import { NavController, NavParams ,LoadingController} from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { LeaddetailPage } from '../leaddetail/leaddetail';
import { StorageProvider } from '../../providers/storage/storage';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
	
	 leadlist: any;
  errorMessage: any;
  selectedItem: any;


  constructor(private storageService:StorageProvider,public navCtrl: NavController, public navParams: NavParams, public rest : RestProvider,public loadingCtrl: LoadingController) {
   
   this.getCountries();
  }
  
  getCountries() {
	  let loading = this.loadingCtrl.create({
        content: 'Please wait...'
       });

     loading.present();
	 
    this.rest.getleadList().subscribe(
	   res => {
	    if(res.status ==200){ 
			  this.leadlist =  res.data;
		   }
	   loading.dismiss();
	   },
	 error =>  {this.errorMessage = <any>error;
	  loading.dismiss();
	   if(this.errorMessage.status == 401){ 
	          //alert(this.errorMessage.message.error);
		      this.storageService.setObject("tokan", '');
			  this.navCtrl.setRoot(LoginPage);
	   }
	  
	 });
  }

  leaddetail(lid) {
   this.navCtrl.push(LeaddetailPage,{'leadid':lid}); 
   
  }
  
  
  
   back(){
	 this.navCtrl.pop(); 
  }
  
}
