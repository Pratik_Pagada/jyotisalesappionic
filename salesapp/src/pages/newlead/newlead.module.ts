import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewleadPage } from './newlead';

@NgModule({
  declarations: [
    NewleadPage,
  ],
  imports: [
    IonicPageModule.forChild(NewleadPage),
  ],
  exports: [
    NewleadPage
  ]
})
export class NewleadPageModule {}
