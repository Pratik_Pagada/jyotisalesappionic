import { Component ,ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams ,LoadingController,Content,ToastController,ModalController} from 'ionic-angular';
import {Validators, FormBuilder, FormGroup,FormControl } from '@angular/forms';
import { RestProvider } from '../../providers/rest/rest';
import { ModalComponent } from '../../components/modal/modal';
import { CompititorComponent } from '../../components/compititor/compititor';
import { StorageProvider } from '../../providers/storage/storage';
import { LoginPage } from '../login/login';
import { ListPage } from '../list/list';
/**
 * Generated class for the NewleadPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-newlead',
  templateUrl: 'newlead.html',
})
export class NewleadPage {
	@ViewChild(Content) content: Content;
	errorMessage:any;
	 loading : any;
	 allDropList:any;
	 customertypelist:any;
	 leadsourcelist:any;
	 customerList:any;
	copititorList:any;
	comapnyslct:any;
	 private leadform : FormGroup;
	competitostatus:any;
	competitostatus1:any;
	competitostatus2:any;
	competitostatus3:any;
    competitostatus4:any;
	comapnyslctid:any;
	customerObj:any;
	crntdate:any;
	addcustomerstatus:any;
	addcustomerstatus1:any;
	addcustomerstatus2:any;
    addcustomerstatus3:any;
	addcustomerstatus4:any;
	 maxdate:any;
	competitorLabel_1:any;
	competitorLabel_2:any;
	competitorLabel_3:any;
	competitorLabel_4:any;
	competitorLabel_5:any;
	
	competitorId_1:any;
	competitorId_2:any;
	competitorId_3:any;
	competitorId_4:any;
	competitorId_5:any;
	
  constructor(private storageService:StorageProvider,public navCtrl: NavController, public navParams: NavParams,private formBuilder: FormBuilder, public rest : RestProvider,public loadingCtrl: LoadingController,private toastCtrl: ToastController,public modalCtrl: ModalController) {
	  
	  this.competitostatus=false;
	   this.competitostatus1=false;
	    this.competitostatus2=false;
		 this.competitostatus3=false;
		  this.competitostatus4=false;
		  
		  
		   this.addcustomerstatus=true;
	   this.addcustomerstatus1=false;
	    this.addcustomerstatus2=false;
		 this.addcustomerstatus3=false;
		  this.addcustomerstatus4=false;
		  
	  this.customerObj ={};
	  
	  this.leadform = this.formBuilder.group({
	   
        comapnyname: ['', Validators.required],
		fullname_1: ['', Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z ]*')])],
		email_1: ['', Validators.compose([Validators.required,Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')])],
		contact_1: ['', Validators.required],
		
		fullname_2:  [''],
		email_2:  [''],
		contact_2:  [''],
		
		fullname_3:  [''],
		email_3:  [''],
		contact_3:  [''],
		
		fullname_4:  [''],
		email_4:  [''],
		contact_4:  [''],
		
		fullname_5:  [''],
		email_5:  [''],
		contact_5:  [''],
		
				
		leadsource: ['', Validators.required],
        customertype: ['',Validators.required],
		competitor_1: ['',Validators.required],
		machinetype_1: ['',Validators.required],
		quantity_1: ['',Validators.required],
		competitor_2:  [''],
		machinetype_2: [''],
		quantity_2:  [''],
		competitor_3:  [''],
		machinetype_3:  [''],
		quantity_3:  [''],
		competitor_4:  [''],
		machinetype_4:  [''],
		quantity_4: [''],
		competitor_5: [''],
		machinetype_5:  [''],
		quantity_5:  [''],
		secondaryphone: [''],
		website:[''],
		remarks: [''],
		nextvisit:['']
		
     });
	  
	 this.getdropdownlist(); 
	 this.getcompanyList();
	 this.getcopititorList();
	 
	 
	 let currentdate = new Date();
	 let crntDay =currentdate.getDate();
	 let crntMonth = currentdate.getMonth();
	 crntMonth = crntMonth+1;
	 let maxyear = currentdate.getFullYear();
	 maxyear = maxyear+5;
	   
   if(crntDay<10 && crntMonth<10){this.maxdate = maxyear+"-0"+crntMonth+"-0"+crntDay;}
   else if (crntDay<10 && crntMonth>9){this.maxdate = maxyear+"-"+crntMonth+"-0"+crntDay;}
   else if (crntDay>9 && crntMonth<10){this.maxdate = maxyear+"-0"+crntMonth+"-"+crntDay;}
   else{this.maxdate = maxyear+"-"+crntMonth+"-"+crntDay; }
	 
  }
  
  back(){
	 this.navCtrl.pop(); 
  }

  ionViewDidLoad() {
	console.log('ionViewDidLoad NewleadPage');
	let today = new Date();
    let dd = today.getDate();
   let mm = today.getMonth()+1; 
   let yyyy = today.getFullYear();
   
   if(dd<10 && mm<10){this.crntdate = yyyy+"-0"+mm+"-0"+dd;}
   else if (dd<10 && mm>9){this.crntdate = yyyy+"-"+mm+"-0"+dd;}
   else if (dd>9 && mm<10){this.crntdate = yyyy+"-0"+mm+"-"+dd;}
   else{this.crntdate = yyyy+"-"+mm+"-"+dd; }
   
  }
  
  changecusttype(ctval){
	  
	  if(ctval==45){ 
		this.leadform.get('competitor_1').enable();
		this.leadform.get('machinetype_1').enable();
		this.leadform.get('quantity_1').enable();

		
		
		  this.competitostatus=true;
	  }else if(ctval==44){ 
		this.leadform.get('competitor_1').enable();
		this.leadform.get('machinetype_1').enable();
		this.leadform.get('quantity_1').enable();

		

		  this.competitostatus=true;
	  }else{
		this.leadform.get('competitor_1').disable();
		this.leadform.get('machinetype_1').disable();
		this.leadform.get('quantity_1').disable();
	
		  this.competitostatus=false; 
		   this.competitostatus1 = false;
		  this.competitostatus2 = false;	
		  this.competitostatus3 = false;	
		  this.competitostatus4 = false;
	  }
  }
  
  selectGotram() {  
     
       let data = { "title": "Company", "data": this.customerList }
        let modal = this.modalCtrl.create(ModalComponent, data);
        modal.onDidDismiss(data => {
			
          if (data) {
           this.comapnyslct = data.company;
		   this.comapnyslctid = data.id;
		   this.getcustomerdtl(this.comapnyslctid);
          }
        });
        modal.present();
}



  selectcompetitor_1() {  
     
       let data = { "title": "Company", "data":  this.copititorList }
        let modal = this.modalCtrl.create(CompititorComponent, data);
        modal.onDidDismiss(data => {
			
          if (data) {
           this.competitorLabel_1 = data.vCompetitorName;
		   this.competitorId_1 = data.iCompetitorListId;
		  
          }
        });
        modal.present();
}

  selectcompetitor_2() {  
     
       let data = { "title": "Company", "data":  this.copititorList }
        let modal = this.modalCtrl.create(CompititorComponent, data);
        modal.onDidDismiss(data => {
			
          if (data) {
           this.competitorLabel_2 = data.vCompetitorName;
		   this.competitorId_2 = data.iCompetitorListId;
		  
          }
        });
        modal.present();
}

  selectcompetitor_3() {  
     
       let data = { "title": "Company", "data":  this.copititorList }
        let modal = this.modalCtrl.create(CompititorComponent, data);
        modal.onDidDismiss(data => {
			
          if (data) {
           this.competitorLabel_3 = data.vCompetitorName;
		   this.competitorId_3 = data.iCompetitorListId;
		  
          }
        });
        modal.present();
}

 selectcompetitor_4() {  
     
       let data = { "title": "Company", "data":  this.copititorList }
        let modal = this.modalCtrl.create(CompititorComponent, data);
        modal.onDidDismiss(data => {
			
          if (data) {
           this.competitorLabel_4 = data.vCompetitorName;
		   this.competitorId_4 = data.iCompetitorListId;
		  
          }
        });
        modal.present();
}


 selectcompetitor_5() {  
     
       let data = { "title": "Company", "data":  this.copititorList }
        let modal = this.modalCtrl.create(CompititorComponent, data);
        modal.onDidDismiss(data => {
			
          if (data) {
           this.competitorLabel_5 = data.vCompetitorName;
		   this.competitorId_5 = data.iCompetitorListId;
		  
          }
        });
        modal.present();
}



  getcustomerdtl(cid){ 
    this.rest.customerDetail(cid).subscribe(data => {
		   if(data.status ==200){ 
			  this.customerObj =  data.data[0];
			 
		   }
	   },
	 error =>  {this.errorMessage = <any>error;
	       this.hideLoading();
		    if(this.errorMessage.status == 401){ 
	          this.storageService.setObject("tokan", '');
			  this.navCtrl.setRoot(LoginPage);
	       }
			this.showToast(this.errorMessage.message.error);
			
	 });
   }


  
  
  AddCompetitor(){
	    if(this.competitostatus3==true){
			this.competitostatus4 = true;
			
		}else if(this.competitostatus2==true){
			this.competitostatus3 = true;
		
		}else if(this.competitostatus1==true){
			this.competitostatus2 = true;
			
		}else if(this.competitostatus==true){
			this.competitostatus1 = true;
		
		}
	  
 }
 
 
  AddCustomer(){
	    if(this.addcustomerstatus3==true){
			this.addcustomerstatus4 = true;
			
		}else if(this.addcustomerstatus2==true){
			this.addcustomerstatus3 = true;
		
		}else if(this.addcustomerstatus1==true){
			this.addcustomerstatus2 = true;
			
		}else if(this.addcustomerstatus==true){
			this.addcustomerstatus1 = true;
		
		}
	  
 }
 
 
 
 
  
  
  deletecompetitor(){
	   if(this.competitostatus4==true){
			this.competitostatus4 = false;
		

		}else if(this.competitostatus3==true){
			this.competitostatus3 = false;
			
			
		}else if(this.competitostatus2==true){
			this.competitostatus2 = false;
			
		
		}else if(this.competitostatus1==true){
			this.competitostatus1 = false;

			
		}
 }
 
 
   deletecustomer(){
	   if(this.addcustomerstatus4==true){
			this.addcustomerstatus4 = false;
		

		}else if(this.addcustomerstatus3==true){
			this.addcustomerstatus3 = false;
			
			
		}else if(this.addcustomerstatus2==true){
			this.addcustomerstatus2 = false;
			
		
		}else if(this.addcustomerstatus1==true){
			this.addcustomerstatus1 = false;

			
		}
 }
 
  
  
   getdropdownlist(){
	   this.showLoading();
	 this.rest.getleaddropdown().subscribe(data => {
		   this.allDropList = data;
	     
		   if(this.allDropList.status ==200){ 
			 for(let d =0; d<this.allDropList.data.length; d++){
				 if(this.allDropList.data[d].id==12){
					 this.customertypelist = this.allDropList.data[d].optionvalues;
				 }else if(this.allDropList.data[d].id==9){
					 this.leadsourcelist = this.allDropList.data[d].optionvalues;
				 }
			 }
		   }
	   },
	 error =>  {this.errorMessage = <any>error;
	      
			this.showToast(this.errorMessage.message.error);
		
			
	 });
	 
	  
  }
 
 
 getcompanyList(){ 
   this.rest.getcustomerList().subscribe(data => {
		   this.hideLoading();
		   this.allDropList = data;
	       if(this.allDropList.status ==200){ 
			  this.customerList =  this.allDropList.data;
		   }
	   },
	 error =>  {this.errorMessage = <any>error;
	       this.hideLoading();
			this.showToast(this.errorMessage.message.error);
	 });
 }
 
  getcopititorList(){ 
  
     this.rest.getcopititorList().subscribe(data => {
		 
		   this.allDropList = data;
	       if(this.allDropList.status ==200){ 
			  this.copititorList =  this.allDropList.data;
		   }
	   },
	   error =>  {this.errorMessage = <any>error;
	       
			this.showToast(this.errorMessage.message.error);
	 });
	 
 }
  
  
  
 validateAllFormFields(formGroup: FormGroup) {         
 Object.keys(formGroup.controls).forEach(field => {  
   const control = formGroup.get(field);            
   if (control instanceof FormControl) {             
	 control.markAsTouched({ onlySelf: true });
   } else if (control instanceof FormGroup) {       
	 this.validateAllFormFields(control);           
   }
 });
}
  
  
  leadformSubmit(){
	if (this.leadform.valid) {
		this.showLoading();
		this.rest.addLead(this.leadform.value,this.comapnyslctid,this.competitorId_1,this.competitorId_2,this.competitorId_3,this.competitorId_4,this.competitorId_5	).subscribe(data => {
			  this.hideLoading();
			  if(data.status ==200){ 
				this.leadform.reset();
				this.showToast(data.message.success)
				this.navCtrl.push(ListPage);
			  }
		  },
		  error =>  {this.errorMessage = <any>error;
			   this.hideLoading();
			    if(this.errorMessage.status == 401){ 
					  this.storageService.setObject("tokan", '');
					  this.navCtrl.setRoot(LoginPage);
				   }
			   this.showToast(this.errorMessage.message.error);
		});
		 
	  } else {
		this.validateAllFormFields(this.leadform); 
		this.content.scrollToTop();
	  }
	   
	  
  }
  
  
    showLoading(){
	  this.loading = this.loadingCtrl.create({
        content: 'Please wait...'
       });
	    this.loading.present();
  }
  
  hideLoading(){
	  this.loading.dismiss();
  }
  
  showToast(msg) {
	  let toast = this.toastCtrl.create({
		message: msg,
		duration: 2000,
		position: 'top'
	  });
      toast.present();
  }
  

}
