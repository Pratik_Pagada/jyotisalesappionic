import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DvrdetailPage } from './dvrdetail';

@NgModule({
  declarations: [
    DvrdetailPage,
  ],
  imports: [
    IonicPageModule.forChild(DvrdetailPage),
  ],
  exports: [
    DvrdetailPage
  ]
})
export class DvrdetailPageModule {}
