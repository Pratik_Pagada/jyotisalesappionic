import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController ,ToastController,ModalController} from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { EnddvrpopupComponent } from '../../components/enddvrpopup/enddvrpopup';
import { StorageProvider } from '../../providers/storage/storage';
import { LocationProvider } from '../../providers/location/location';
import { Geolocation } from '@ionic-native/geolocation';
import { Platform } from 'ionic-angular';
import { DvrlocationComponent } from '../../components/dvrlocation/dvrlocation';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { LoginPage } from '../login/login';



/**
 * Generated class for the DvrdetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-dvrdetail',
  templateUrl: 'dvrdetail.html',
})
export class DvrdetailPage {
 
 dvrObj:any;
  dvrid:any;
 locationlist:any;
 localLocations: any = [];
  
   dvrstatus: any;
  transportList:any;
  fuielList:any;
  loading:any;
  errorMessage:any;
  enddvrObj:any;
  locationObj:any;
  dvrpinstatus:any;
  comapnytypeList:any;

  visitTypeList:any;
  allDropList:any;
  customerlocation:any;
  visittype:any;
  purpose:any;
  dvrLocObj:any;
  customerList:any;
  leadlist:any;
  pinlocationno:any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams,public rest : RestProvider,public loadingCtrl: LoadingController,public modalCtrl: ModalController,private storageService:StorageProvider,private platform: Platform, private geolocation: Geolocation,private toastCtrl: ToastController,private locationService:LocationProvider,private sqlite: SQLite) {
	  
	  this.dvrid =   this.navParams.get('did');
	   this.getcompanyList();
	   this.getLeadlist();
	  this.getdvrdetail();
	  this.dvrObj={};
	 
	  this.getdvrLocationdetail();
	   this.gettransporttype();
	
	  this.enddvrObj ={};
	  this.locationObj ={};
	  this.dvrpinstatus=false;
	  this.comapnytypeList = [];
	  this.customerList = [];
	  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DvrdetailPage');
  }
  
  
  
    
  pinlocation(){
	  this.dvrpinstatus=false;
	  this.addmorecustomerloc();
  }
  
  
   getcompanyList(){ 
   this.rest.getcustomerList().subscribe(data => {
		
	       if(data.status ==200){ 
			  this.customerList =  data.data;
		   }
	   },
	 error =>  {this.errorMessage = <any>error;
	     
	 });
 }

 getLeadlist() {
   
  this.rest.getleadList1().subscribe(
	 res => {
	  if(res.status ==200){ 
			this.leadlist =  res.data;
		 }
		 this.hideLoading();
	 },
   error =>  {this.errorMessage = <any>error;
	this.hideLoading();
   });
}

  

  gettransporttype(){
	  this. showLoading();
	  this.rest.getdvrdroplist().subscribe(data => {
		  
		   this.allDropList = data;
	   
		   if(this.allDropList.status ==200){ 
			 for(let d =0; d<this.allDropList.data.length; d++){
				 if(this.allDropList.data[d].id==19){
					 this.transportList = this.allDropList.data[d].optionvalues;
				 }else if(this.allDropList.data[d].id==20){
					 this.fuielList = this.allDropList.data[d].optionvalues;
				 }else if(this.allDropList.data[d].id==31){
					 this.visitTypeList = this.allDropList.data[d].optionvalues;
				 }
			 }
		   }
		   
		  
		 },
	   error =>  {this.errorMessage = <any>error;
	       
			this.showToast(this.errorMessage.message.error);
	 });
  }
  
  
  
  enddvr(){
       this.platform.ready().then(() => {
		   this.showLoading();
		  let opt ={maximumAge:9000,timeout:9000,enableHighAccuracy:true};   
      
		  this.geolocation.getCurrentPosition(opt).then((resp) => {
			  this.locationObj.lat = resp.coords.latitude;
			  this.locationObj.long = resp.coords.longitude;
	        this.hideLoading();
			this.enddvrpopup();
			
			let currentdate = new Date();
		let crntMonth = currentdate.getMonth();
		crntMonth = crntMonth+1;
		
		let datetime =  currentdate.getDate() + "/"+ crntMonth + "/" + currentdate.getFullYear() + " " + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
			
			this.sqlite.create({
				name: 'salesdb.db',
				location: 'default'
			  }).then((db: SQLiteObject) => {
				db.executeSql('INSERT INTO locationdata VALUES(NULL,?,?,?)',[this.locationObj.lat,this.locationObj.long,datetime])
				  .then(res => {
					console.log(res);
					
				  })
				  .catch(e => {
					console.log(e);
				   
				  });
			  }).catch(e => {
				console.log(e);
			   
			  });



		  
		}).catch((error) => {
				this.enddvr();
				 this.hideLoading();
			   alert('Error getting location');
			});

	 });  
		  
  }
  
  
  deleteLocations(){
	  
	  this.sqlite.create({
		name: 'salesdb.db',
		location: 'default'
	  }).then((db: SQLiteObject) => {
				
		db.executeSql('DELETE  FROM locationdata', {})
		.then(res => {
		   // alert(JSON.stringify(res))
         })
		.catch(e => console.log(e));

	});
	  
	  
  }
  
  
  
  
  
   enddvrpopup() {  
      this.enddvrObj.transport = this.transportList;
	  this.enddvrObj.fuiel = this.fuielList;
	  
       let data = { "title": "dvr", "data": this.enddvrObj }
        let modal = this.modalCtrl.create(EnddvrpopupComponent, data);
        modal.onDidDismiss(data => {
		  if (data) {
			   if(data.vehicletype){
				   this.dvrstatus="start";
						 this.showLoading();		 
				         this.storageService.setObject("dvrstatus", "start");
				 
				 	this.sqlite.create({
						name: 'salesdb.db',
						location: 'default'
					  }).then((db: SQLiteObject) => {
								
						db.executeSql('SELECT * FROM locationdata', {})
						.then(res => {
						  this.localLocations = [];
						  for(var i=0; i<res.rows.length; i++) {
							this.localLocations.push({lat:res.rows.item(i).lat,long:res.rows.item(i).long,time:res.rows.item(i).time})
						  }
						 
						//  alert(JSON.stringify(this.localLocations));
				
						   this.rest.enddvrSubmt(this.locationObj.lat,this.locationObj.long,this.dvrid,data,this.localLocations).subscribe(data => {
							   this.hideLoading();
							   if(data.status ==200){ 
							   this.dvrObj.eRunningStatus ="End";
								this.showToast(data.message.success);
								this.storageService.setObject("dvrid", "");
								 this.locationService.stopTracking();
								 this.deleteLocations();
								 this.getdvrdetail();
								 }
							 },
						   error =>  {this.errorMessage = <any>error;
								this.hideLoading();
								 if(this.errorMessage.status == 401){ 
									  this.storageService.setObject("tokan", '');
									  this.navCtrl.setRoot(LoginPage);
							    }
								this.showToast(this.errorMessage.message.error);
						 });
						 
						 
						 
						 
						 })
		                .catch(e => console.log(e));
						 
				 
			 });
		  }
		}
        });
        modal.present();
 }
 
 
    addmorecustomerloc(){ 
		this.platform.ready().then(() => {
			   this.showLoading();
			  let opt ={maximumAge:9000,timeout:9000,enableHighAccuracy:true};   
		  
			  this.geolocation.getCurrentPosition(opt).then((resp) => {
				  this.locationObj.lat = resp.coords.latitude;
				  this.locationObj.long = resp.coords.longitude;
				
				this.addmorecustomerpinpoint();
				
				
		let currentdate = new Date();
		let crntMonth = currentdate.getMonth();
		crntMonth = crntMonth+1;
		
		let datetime =  currentdate.getDate() + "/"+ crntMonth + "/" + currentdate.getFullYear() + " " + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
			
			this.sqlite.create({
				name: 'salesdb.db',
				location: 'default'
			  }).then((db: SQLiteObject) => {
				db.executeSql('INSERT INTO locationdata VALUES(NULL,?,?,?)',[this.locationObj.lat,this.locationObj.long,datetime])
				  .then(res => {
					console.log(res);
					
				  })
				  .catch(e => {
					console.log(e);
				   
				  });
			  }).catch(e => {
				console.log(e);
			   
			  });

				
				
				
			  
			}).catch((error) => {
					this.addmorecustomerloc();
					 this.hideLoading();
				   alert('Error getting location');
				});

		 });  
 
	}
 
   addmorecustomerpinpoint(){ 
	

		  this.dvrpinstatus=false;
		  
			this.rest.pinlocationSubmt(this.dvrid,this.locationObj.lat,this.locationObj.long,this.pinlocationno).subscribe(data => {
				this.hideLoading();
				if(data.status ==200){ 
				
				 this.showToast(data.message.success);
				 this.getdvrLocationdetail();
				 
				  
				  }
			  },
			error =>  {this.errorMessage = <any>error;
				 this.hideLoading();
				  if(this.errorMessage.status == 401){ 
						  this.storageService.setObject("tokan", '');
						  this.navCtrl.setRoot(LoginPage);
					}
				this.showToast(this.errorMessage.message.error);
		  });



	




	 /*   if(this.customerlocation){
			if(this.visittype){
				if(this.purpose){ */
					
				/*
				}
			}
        } */
	 }
 
  
  
  
  dvrdetail(locationobj){
	  this.dvrLocObj ={};
	  
	   this.dvrLocObj.locationobj = locationobj;
	    this.dvrLocObj.custmrlist = this.customerList;
		 this.dvrLocObj.visittypelist = this.visitTypeList;
		 this.dvrLocObj.leadlist =  this.leadlist;
	  
	  let data = { "title": "dvr", "data": this.dvrLocObj }
        let modal = this.modalCtrl.create(DvrlocationComponent, data);
        modal.onDidDismiss(data => {
		  if (data) {
			   if(data.visittype){
				  this.showLoading();
				
				   this.rest.dvrpinlocadd(data,this.dvrid).subscribe(data => {
					  
					   if(data.status ==200){ 
					      this.getdvrLocationdetail1();
						  this.showToast(data.message.success);
						
						 }
					 },
				   error =>  {this.errorMessage = <any>error;
						this.hideLoading();
						if(this.errorMessage.status == 401){ 
						  this.storageService.setObject("tokan", '');
						  this.navCtrl.setRoot(LoginPage);
					    }
						this.showToast(this.errorMessage.message.error);
				 });
				 
			 }
          }
        });
        modal.present();
	  
  }
  
  
  
  
   getdvrdetail(){
	  

    this.rest.dvrDetail(this.dvrid).subscribe(
	   res => { 
	    if(res.status ==200){ 
			  this.dvrObj =  res.data[0];
		   }
	   
	   },
	 error =>  {this.errorMessage = <any>error;
	       if(this.errorMessage.status == 401){ 
				  this.storageService.setObject("tokan", '');
				  this.navCtrl.setRoot(LoginPage);
		  }
	 });
	  
  }
  
    getdvrLocationdetail(){ 
		   
        this.rest.getlocationlist(this.dvrid).subscribe(data => {
				   if(data.status ==200){ 
						this.locationlist = data.data;
					if(this.locationlist.length){
						this.pinlocationno = this.locationlist.length + 1;
					}else{
						this.pinlocationno = 1;
					}
					
					}
				 },
			   error =>  {this.errorMessage = <any>error;
					   this.hideLoading();
					   if(this.errorMessage.status == 401){ 
								  this.storageService.setObject("tokan", '');
								  this.navCtrl.setRoot(LoginPage);
						  }
			   });
	}
	
	
	 getdvrLocationdetail1(){ 
		   
            this.rest.getlocationlist(this.dvrid).subscribe(data => {
				   if(data.status ==200){ 
						this.locationlist = data.data;
					
						if(this.locationlist.length){
							this.pinlocationno = this.locationlist.length + 1;
						}else{
							this.pinlocationno = 1;
						}
						 this.hideLoading();
					}
				 },
			   error =>  {this.errorMessage = <any>error;
					   this.hideLoading();
					   if(this.errorMessage.status == 401){ 
							  this.storageService.setObject("tokan", '');
							  this.navCtrl.setRoot(LoginPage);
					  }
			   });
	}
	
	
	
  showLoading(){
	  this.loading = this.loadingCtrl.create({
        content: 'Please wait...'
       });
	    this.loading.present();
  }
  
  hideLoading(){
	  this.loading.dismiss();
  }
  
  showToast(msg) {
	  let toast = this.toastCtrl.create({
		message: msg,
		duration: 2000,
		position: 'top'
	  });
      toast.present();
  }

	
	
  
   back(){
	 this.navCtrl.pop(); 
  }

}
