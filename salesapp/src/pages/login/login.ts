import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,LoadingController,ToastController,Platform} from 'ionic-angular';
import { HomePage } from '../home/home';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { RestProvider } from '../../providers/rest/rest';
import { StorageProvider } from '../../providers/storage/storage';


/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  
  private loginform : FormGroup;
  loginres:any;
  errorMessage:any;
  loading:any;
  pushtokan:any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder, public rest : RestProvider,public loadingCtrl: LoadingController,private toastCtrl: ToastController ,private plt:Platform,private storageService:StorageProvider) {
   
   this.loginform = this.formBuilder.group({
	 
        name: ['', Validators.required],
		pswd: ['', Validators.required],
		
     });
	 
	
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
	this.storageService.getObject("tokan").then((val)=>{
			if(val){
			 this.navCtrl.setRoot(HomePage);	
			}
	}); 
 /*	this.plt.ready().then(() => { 
    this.push.hasPermission().then((res: any) => {
  
	  if (res.isEnabled) {
		
	  } else {
		console.log('We do not have permission to send push notifications');
	  }
  
	});

   });*/

  }



 /* getpushtokan(){
	this.plt.ready().then(() => {

		const options: PushOptions = {
			android: {
			  senderID: '167026610272'
			},
			ios: {
			  alert: 'true',
			  badge: false,
			  sound: 'true'
			},
			windows: {}
		  };
		  const pushObject: PushObject = this.push.init(options);

		  pushObject.on('registration').subscribe((registration: any) => {
		   alert('Device registered'+ JSON.stringify(registration));

		  }	);

		  pushObject.on('error').subscribe(error =>{ alert('Error with Push plugin'+JSON.stringify(error))
		  
		  });

	});
  }
*/







  
  loginbtn(){ 
       let platforms =  '';
	   if (this.plt.is('ios')) {
		     platforms =  'ios';
	   }else{
		   platforms =  'Android';
	   }
		 this.showLoading();
	
	this.rest.login(this.loginform.value,platforms).subscribe(
	   data => {this.loginres = data;
	    
		 this.hideLoading();
		 this.loginform.reset();
		 if(this.loginres.status ==200){ 
			 this.storageService.setObject("tokan", this.loginres.data.vAuthToken);
			 this.showToast(this.loginres.message.success);
			
			 this.navCtrl.setRoot(HomePage);
		 }
	   },
	 error =>  {this.errorMessage = <any>error;
	        this.hideLoading();
			this.showToast(this.errorMessage.message.error);
			this.loginform.reset();
	 });
  
  }
  
  showLoading(){
	  this.loading = this.loadingCtrl.create({
        content: 'Please wait...'
       });
	    this.loading.present();
  }
  
  hideLoading(){
	  this.loading.dismiss();
  }
  
  showToast(msg) {
	  let toast = this.toastCtrl.create({
		message: msg,
		duration: 3000,
		position: 'top'
	  });
      toast.present();
  }
  
  
}
