/*import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController ,ToastController,ModalController} from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { EnddvrpopupComponent } from '../../components/enddvrpopup/enddvrpopup';
import { StorageProvider } from '../../providers/storage/storage';
import { Geolocation } from '@ionic-native/geolocation';
import { DvrPage } from '../dvr/dvr';
import { LocationProvider } from '../../providers/location/location';
import { Platform } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { LoginPage } from '../login/login';




@IonicPage()
@Component({
  selector: 'page-adddvr',
  templateUrl: 'adddvr.html',
})
export class AdddvrPage {

  dvrstatus: any;
  transportList:any;
  fuielList:any;
  loading:any;
  errorMessage:any;
  enddvrObj:any;
  locationObj:any;
  dvrpinstatus:any;
  comapnytypeList:any;
  dvrid:any;
  visitTypeList:any;
  allDropList:any;
  customerlocation:any;
  visittype:any;
  purpose:any;

  
  
  constructor(public navCtrl: NavController, public navParams: NavParams,public rest : RestProvider, public loadingCtrl: LoadingController,private toastCtrl: ToastController,public modalCtrl: ModalController,private storageService:StorageProvider,private platform: Platform, private geolocation: Geolocation,private locationService:LocationProvider,private sqlite: SQLite) {
	 
	
	  this.enddvrObj ={};
	  this.locationObj ={};
	  this.dvrpinstatus=false;
	  this.comapnytypeList = [];
	
	  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdddvrPage');
	
	
  }
  


  
  
  back(){
	 this.navCtrl.pop(); 
  }
  
  startdvr(){
	  
	    this.platform.ready().then(() => {

		//	this.storageService.getObject("dvrid").then((val)=>{

				
				

					let opt ={maximumAge:9000,timeout:9000,enableHighAccuracy:true};   
					
						this.geolocation.getCurrentPosition(opt).then((resp) => {
						  
							this.locationObj.lat = resp.coords.latitude;
							this.locationObj.long = resp.coords.longitude;
							
							
							this.showLoading();
							 this.rest.startdvrSubmt(this.locationObj.lat,this.locationObj.long).subscribe(data => {
								 this.hideLoading();
								 if(data.status ==200){ 
								 
								 	let currentdate = new Date();
									let crntMonth = currentdate.getMonth();
									crntMonth = crntMonth+1;
									
									let datetime =  currentdate.getDate() + "/"+ crntMonth + "/" + currentdate.getFullYear() + " " + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();


                  this.sqlite.create({
										name: 'salesdb.db',
										location: 'default'
									  }).then((db: SQLiteObject) => {
										db.executeSql('INSERT INTO locationdata VALUES(NULL,?,?,?)',[this.locationObj.lat,this.locationObj.long,datetime])
										  .then(res => {
											console.log(res);
											
										  })
										  .catch(e => {
											console.log(e);
										   
										  });
									  }).catch(e => {
										console.log(e);
									   
									  });

								 
									  this.showToast(data.message.success);
									  this.storageService.setObject("dvrid", data.data.iDvrId);
									  this.locationService.startTracking();
									  this.navCtrl.push(DvrPage);
									  
								   }
							   },
							 error =>  {this.errorMessage = <any>error;
							   if(this.errorMessage.status == 401){ 
	                                  this.storageService.setObject("tokan", '');
									  this.navCtrl.setRoot(LoginPage);
							   }
								  this.hideLoading();
								  this.showToast(this.errorMessage.message.error);
						   });
							
							
							
						   
						  }).catch((error) => {
							  this.hideLoading();
							  this.startdvr();
							// alert('Error getting location');
						  });
				
		  // }); 
		   
		 

		});
	  
	  
	   
	   
  }
  
  
  
  
  
  
  
  
  
   showLoading(){
	  this.loading = this.loadingCtrl.create({
        content: 'Please wait...'
       });
	    this.loading.present();
  }
  
  hideLoading(){
	  this.loading.dismiss();
  }
  
  showToast(msg) {
	  let toast = this.toastCtrl.create({
		message: msg,
		duration: 3000,
		position: 'top'
	  });
      toast.present();
  }

}
*/