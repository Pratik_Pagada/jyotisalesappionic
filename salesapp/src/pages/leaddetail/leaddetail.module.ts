import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LeaddetailPage } from './leaddetail';

@NgModule({
  declarations: [
    LeaddetailPage,
  ],
  imports: [
    IonicPageModule.forChild(LeaddetailPage),
  ],
  exports: [
    LeaddetailPage
  ]
})
export class LeaddetailPageModule {}
