import { Component ,ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams ,LoadingController,Content,ToastController,ModalController} from 'ionic-angular';
import {Validators, FormBuilder, FormGroup,FormControl } from '@angular/forms';
import { RestProvider } from '../../providers/rest/rest';
import { ModalComponent } from '../../components/modal/modal';
import { AddproductPage } from '../addproduct/addproduct';
import { StorageProvider } from '../../providers/storage/storage';
import { LoginPage } from '../login/login';
import { CompititorComponent } from '../../components/compititor/compititor';


/**
 * Generated class for the LeaddetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-leaddetail',
  templateUrl: 'leaddetail.html',
})
export class LeaddetailPage {
@ViewChild(Content) content: Content;
  leadid:any;
  leadObj:any

	errorMessage:any;
	 loading : any;
	 allDropList:any;
	 customertypelist:any;
	 leadsourcelist:any;
	 customerList:any;
	copititorList:any;
	comapnyslct:any;
	 private editleadform : FormGroup;
	competitostatus:any;
	competitostatus1:any;
	competitostatus2:any;
	competitostatus3:any;
    competitostatus4:any;
	comapnyslctid:any;
	customerObj:any;
    leadform:any;
	compitiornew:number;
	contactnew:number;
    crntdate:any;
    addcustomerstatus:any;
	addcustomerstatus1:any;
	addcustomerstatus2:any;
    addcustomerstatus3:any;
	addcustomerstatus4:any;
    maxdate:any;
	
	competitorLabel_1:any;
	competitorLabel_2:any;
	competitorLabel_3:any;
	competitorLabel_4:any;
	competitorLabel_5:any;
	
	competitorId_1:any;
	competitorId_2:any;
	competitorId_3:any;
	competitorId_4:any;
	competitorId_5:any;
	
  constructor(private storageService:StorageProvider,public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder, public rest : RestProvider,public loadingCtrl: LoadingController,private toastCtrl: ToastController,public modalCtrl: ModalController) {
	  
	this.leadid =   this.navParams.get('leadid');
	
	this.leadObj={};
	this.compitiornew=0;
	this.contactnew =0;
	 this.competitostatus=false;
	   this.competitostatus1=false;
	    this.competitostatus2=false;
		 this.competitostatus3=false;
		  this.competitostatus4=false;
		  
       this.addcustomerstatus=true;
	   this.addcustomerstatus1=false;
	    this.addcustomerstatus2=false;
		 this.addcustomerstatus3=false;
		  this.addcustomerstatus4=false;		
		
	  this.customerObj ={};
	  this.leadform = {};
	  this.editleadform = this.formBuilder.group({
	   
          comapnyname: ['', Validators.required],
		fullname_1: ['',Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z ]*')])],
		email_1: ['',Validators.compose([Validators.required,Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')])],
		contact_1: ['', Validators.required],
		
		fullname_2:  [''],
		email_2:  [''],
		contact_2:  [''],
		
		fullname_3:  [''],
		email_3:  [''],
		contact_3:  [''],
		
		fullname_4:  [''],
		email_4:  [''],
		contact_4:  [''],
		
		fullname_5:  [''],
		email_5:  [''],
		contact_5:  [''],
		
				
		leadsource: ['', Validators.required],
        customertype: ['',Validators.required],
		competitor_1: ['',Validators.required],
		machinetype_1: ['',Validators.required],
		quantity_1: ['',Validators.required],
		competitor_2:  [''],
		machinetype_2: [''],
		quantity_2:  [''],
		competitor_3:  [''],
		machinetype_3:  [''],
		quantity_3:  [''],
		competitor_4:  [''],
		machinetype_4:  [''],
		quantity_4: [''],
		competitor_5: [''],
		machinetype_5:  [''],
		quantity_5:  [''],
		secondaryphone: [''],
		website:[''],
		remarks: [''],
		nextvisit:['']
		
     });
	  
	 
	 this.getdropdownlist(); 
	 this.getcompanyList();
	 
	 
	this.getleaddetail();
	
	let currentdate = new Date();
	let crntDay =currentdate.getDate();
	let crntMonth = currentdate.getMonth();
	crntMonth = crntMonth+1;
	let maxyear = currentdate.getFullYear();
	maxyear = maxyear+5;
	  
  if(crntDay<10 && crntMonth<10){this.maxdate = maxyear+"-0"+crntMonth+"-0"+crntDay;}
  else if (crntDay<10 && crntMonth>9){this.maxdate = maxyear+"-"+crntMonth+"-0"+crntDay;}
  else if (crntDay>9 && crntMonth<10){this.maxdate = maxyear+"-0"+crntMonth+"-"+crntDay;}
  else{this.maxdate = maxyear+"-"+crntMonth+"-"+crntDay; }
	
  }

  ionViewDidLoad() {
	console.log('ionViewDidLoad LeaddetailPage');
	
	let today = new Date();
    let dd = today.getDate();
   let mm = today.getMonth()+1; 
   let yyyy = today.getFullYear();
   
   if(dd<10 && mm<10){this.crntdate = yyyy+"-0"+mm+"-0"+dd;}
   else if (dd<10 && mm>9){this.crntdate = yyyy+"-"+mm+"-0"+dd;}
   else if (dd>9 && mm<10){this.crntdate = yyyy+"-0"+mm+"-"+dd;}
   else{this.crntdate = yyyy+"-"+mm+"-"+dd; }
	
  }
  
  addPrdoct(){
	  this.navCtrl.push(AddproductPage,{"leadobj":this.leadObj});  
	  
  }
  
  
  
   
  changecusttype(ctval){
	  
	  if(ctval==45){ 
		  this.competitostatus=true;
		  this.editleadform.get('competitor_1').enable();
		this.editleadform.get('machinetype_1').enable();
		this.editleadform.get('quantity_1').enable();

		
	  }else if(ctval==44){ 
		  this.competitostatus=true;
		  this.editleadform.get('competitor_1').enable();
		this.editleadform.get('machinetype_1').enable();
		this.editleadform.get('quantity_1').enable();

	  }else{
		  this.editleadform.get('competitor_1').disable();
		this.editleadform.get('machinetype_1').disable();
		this.editleadform.get('quantity_1').disable();
		
		  this.competitostatus=false; 
		  this.competitostatus1 = false;
		  this.competitostatus2 = false;	
		  this.competitostatus3 = false;	
		  this.competitostatus4 = false;	
		  
	  }
  }
  
  selectGotram() {  
     
       let data = { "title": "Company", "data": this.customerList }
        let modal = this.modalCtrl.create(ModalComponent, data);
        modal.onDidDismiss(data => {
			
          if (data) {
           this.comapnyslct = data.company;
		   this.comapnyslctid = data.id;
		   this.getcustomerdtl(this.comapnyslctid);
          }
        });
        modal.present();
}


  getcustomerdtl(cid){ 
    this.rest.customerDetail(cid).subscribe(data => {
		   if(data.status ==200){ 
			  this.customerObj =  data.data[0];
		   }
	   },
	 error =>  {this.errorMessage = <any>error;
	       this.hideLoading();
		   if(this.errorMessage.status == 401){ 
	          this.storageService.setObject("tokan", '');
			  this.navCtrl.setRoot(LoginPage);
	       }
			this.showToast(this.errorMessage.message.error);
			
	 });
   }


  
  
  AddCompetitor(){
	    if(this.competitostatus3==true){
			this.competitostatus4 = true;
		}else if(this.competitostatus2==true){
			this.competitostatus3 = true;
		}else if(this.competitostatus1==true){
			this.competitostatus2 = true;
		}else if(this.competitostatus==true){
			this.competitostatus1 = true;
		}
	  
 }
  
  
  
  AddCustomer(){
	    if(this.addcustomerstatus3==true){
			this.addcustomerstatus4 = true;
			
		}else if(this.addcustomerstatus2==true){
			this.addcustomerstatus3 = true;
		
		}else if(this.addcustomerstatus1==true){
			this.addcustomerstatus2 = true;
			
		}else if(this.addcustomerstatus==true){
			this.addcustomerstatus1 = true;
		
		}
	  
 }
 
 
  
  
  deletecompetitor(cid){ 
	   if(this.competitostatus4==true){
			this.competitostatus4 = false;
		}else if(this.competitostatus3==true){
			this.competitostatus3 = false;
		}else if(this.competitostatus2==true){
			this.competitostatus2 = false;
		}else if(this.competitostatus1==true){
			this.competitostatus1 = false;
		}
		
		
		if(cid){
			  this.showLoading();
			  this.rest.deletecompititor(cid).subscribe(data => {
				   this.allDropList = data;
				  this.hideLoading();
				   if(this.allDropList.status ==200){ 
					  this.showToast(this.allDropList.message.success)
				   }
				
			   },
			 error =>  {this.errorMessage = <any>error;
				        this.hideLoading();
						if(this.errorMessage.status == 401){ 
						  this.storageService.setObject("tokan", '');
						  this.navCtrl.setRoot(LoginPage);
					   }
						this.showToast(this.errorMessage.message.error);
					  
					
			 });
			
		}
		
 }
 
 
   deletecustomer(ctid){
	   if(this.addcustomerstatus4==true){
			this.addcustomerstatus4 = false;
		

		}else if(this.addcustomerstatus3==true){
			this.addcustomerstatus3 = false;
			
			
		}else if(this.addcustomerstatus2==true){
			this.addcustomerstatus2 = false;
			
		
		}else if(this.addcustomerstatus1==true){
			this.addcustomerstatus1 = false;
	
		}

		if(ctid){
			this.showLoading();
			this.rest.deletecontact(ctid).subscribe(data => {
				 this.allDropList = data;
				this.hideLoading();
				 if(this.allDropList.status ==200){ 
					this.showToast(this.allDropList.message.success)
				 }
			  
			 },
		   error =>  {this.errorMessage = <any>error;
					  this.hideLoading();
					  if(this.errorMessage.status == 401){ 
						  this.storageService.setObject("tokan", '');
						  this.navCtrl.setRoot(LoginPage);
					   }
						this.showToast(this.errorMessage.message.error);
					 
				  
		   });
		  
	  }

 }
 
 
  
  
   getdropdownlist(){
	   this.showLoading();
	 this.rest.getleaddropdown().subscribe(data => {
		   this.allDropList = data;
	     
		   if(this.allDropList.status ==200){ 
			 for(let d =0; d<this.allDropList.data.length; d++){
				 if(this.allDropList.data[d].id==12){
					 this.customertypelist = this.allDropList.data[d].optionvalues;
				 }else if(this.allDropList.data[d].id==9){
					 this.leadsourcelist = this.allDropList.data[d].optionvalues;
				 }
			 }
		   }
		
	   },
	 error =>  {this.errorMessage = <any>error;
	       
			this.showToast(this.errorMessage.message.error);
			
	 });
	 
	  
  }
 
 
 getcompanyList(){ 
   this.rest.getcustomerList().subscribe(data => {
		 
		   this.allDropList = data;
	       if(this.allDropList.status ==200){ 
			  this.customerList =  this.allDropList.data;
		   }
	   },
	 error =>  {this.errorMessage = <any>error;
	       
			this.showToast(this.errorMessage.message.error);
			
	 });
 }
 
  getcopititorList(){ 
  
     this.rest.getcopititorList().subscribe(data => {
		this.hideLoading();
		   this.allDropList = data;
	       if(this.allDropList.status ==200){ 
			  this.copititorList =  this.allDropList.data;
		   }
	   },
	   error =>  {this.errorMessage = <any>error;
		this.hideLoading();
			this.showToast(this.errorMessage.message.error);
	 });
	 
 }
 
 
 
 
 selectcompetitor_1() {  
	
    
       let data = { "title": "Company", "data":  this.copititorList }
        let modal = this.modalCtrl.create(CompititorComponent, data);
        modal.onDidDismiss(data => {
			
          if (data) {
           this.leadform.competitor_1 = data.vCompetitorName;
		   this.competitorId_1 = data.iCompetitorListId;
		  
          }
        });
        modal.present();
}

  selectcompetitor_2() {  
	
     
       let data = { "title": "Company", "data":  this.copititorList }
        let modal = this.modalCtrl.create(CompititorComponent, data);
        modal.onDidDismiss(data => {
			
          if (data) {
           this.leadform.competitor_2 = data.vCompetitorName;
		   this.competitorId_2 = data.iCompetitorListId;
		  
          }
        });
        modal.present();
}

  selectcompetitor_3() {  
	
     
       let data = { "title": "Company", "data":  this.copititorList }
        let modal = this.modalCtrl.create(CompititorComponent, data);
        modal.onDidDismiss(data => {
			
          if (data) {
           this.leadform.competitor_3 = data.vCompetitorName;
		   this.competitorId_3 = data.iCompetitorListId;
		  
          }
        });
        modal.present();
}

 selectcompetitor_4() {  
     
       let data = { "title": "Company", "data":  this.copititorList }
        let modal = this.modalCtrl.create(CompititorComponent, data);
        modal.onDidDismiss(data => {
			
          if (data) {
           this.leadform.competitor_4 = data.vCompetitorName;
		   this.competitorId_4 = data.iCompetitorListId;
		  
          }
        });
        modal.present();
}


 selectcompetitor_5() {  
     
       let data = { "title": "Company", "data":  this.copititorList }
        let modal = this.modalCtrl.create(CompititorComponent, data);
        modal.onDidDismiss(data => {
			
          if (data) {
           this.leadform.competitor_5 = data.vCompetitorName;
		   this.competitorId_5 = data.iCompetitorListId;
		  
          }
        });
        modal.present();
}

 
  
  
  validateAllFormFields(formGroup: FormGroup) {        
 Object.keys(formGroup.controls).forEach(field => { 
   const control = formGroup.get(field);             
   if (control instanceof FormControl) {            
	 control.markAsTouched({ onlySelf: true });
   } else if (control instanceof FormGroup) {       
	 this.validateAllFormFields(control);            
   }
 });
}
 
  
  
  leadformSubmit(){
	if (this.editleadform.valid) {  
		
		this.showLoading();
		 this.rest.editLead(this.editleadform.value,this.leadform,this.comapnyslctid,this.leadid,this.compitiornew,this.contactnew,this.competitorId_1,this.competitorId_2,this.competitorId_3,this.competitorId_4,this.competitorId_5).subscribe(data => {
			   this.hideLoading();
			   if(data.status ==200){ 
				
				 this.showToast(data.message.success)
			   }
		   },
		   error =>  {this.errorMessage = <any>error;
				this.hideLoading();
				 if(this.errorMessage.status == 401){ 
						  this.storageService.setObject("tokan", '');
						  this.navCtrl.setRoot(LoginPage);
					   }
				this.showToast(this.errorMessage.message.error);
				
		 });
	  
	   } else {
		this.validateAllFormFields(this.editleadform); 
		this.content.scrollToTop();
	  }
  }
  
  getleaddetail() { 
	this.rest.leadDetail(this.leadid).subscribe(
	   res => { 
		this.getcopititorList();
	    if(res.status ==200){ 
			  this.leadObj =  res.data[0];
			
				this.comapnyslctid = res.data[0].iCustomerId;
				let contactlenth =  res.data[0].ContactData.length;
				
				if(contactlenth>0){
					this.leadform.contactid_1 = res.data[0].ContactData[0].iContactId;
					this.leadform.fullname_1 = res.data[0].ContactData[0].vFullName;
					this.leadform.email_1 = res.data[0].ContactData[0].vEmail;
					this.leadform.contact_1 = res.data[0].ContactData[0].vContact;
					this.contactnew = 1;
				}

				if(contactlenth>1){
					this.leadform.contactid_2 = res.data[0].ContactData[1].iContactId;
					this.leadform.fullname_2 = res.data[0].ContactData[1].vFullName;
					this.leadform.email_2 = res.data[0].ContactData[1].vEmail;
					this.leadform.contact_2 = res.data[0].ContactData[1].vContact;
					this.contactnew = 2;
					this.addcustomerstatus1 = true;
				}
			
				if(contactlenth>2){
					this.leadform.contactid_3 = res.data[0].ContactData[2].iContactId;
					this.leadform.fullname_3 = res.data[0].ContactData[2].vFullName;
					this.leadform.email_3 = res.data[0].ContactData[2].vEmail;
					this.leadform.contact_3 = res.data[0].ContactData[2].vContact;
					this.contactnew = 3;
					this.addcustomerstatus1 = true;
					this.addcustomerstatus2 = true;
				}
			
				if(contactlenth>3){
					this.leadform.contactid_4 = res.data[0].ContactData[3].iContactId;
					this.leadform.fullname_4 = res.data[0].ContactData[3].vFullName;
					this.leadform.email_4 = res.data[0].ContactData[3].vEmail;
					this.leadform.contact_4 = res.data[0].ContactData[3].vContact;
					this.contactnew = 4;
					this.addcustomerstatus1 = true;
					this.addcustomerstatus2 = true;
					this.addcustomerstatus3 = true;
				}

				if(contactlenth>4){
					this.leadform.contactid_5 = res.data[0].ContactData[4].iContactId;
					this.leadform.fullname_5 = res.data[0].ContactData[4].vFullName;
					this.leadform.email_5 = res.data[0].ContactData[4].vEmail;
					this.leadform.contact_5 = res.data[0].ContactData[4].vContact;
					this.contactnew = 5;
					this.addcustomerstatus1 = true;
					this.addcustomerstatus2 = true;
					this.addcustomerstatus3 = true;
					this.addcustomerstatus4 = true;
				}
				
			
				this.leadform.customertype = res.data[0].iCustType;
				this.leadform.leadsource = res.data[0].iLeadSourchId;
				this.leadform.secondaryphone = res.data[0].vSecondPhone;
				this.leadform.website = res.data[0].vWebsite;
				this.leadform.remarks = res.data[0].vRemark;
				this.leadform.nextvisit = res.data[0].dNextVisitDate;
				this.comapnyslct = res.data[0].company;
			

		if(this.leadform.customertype==44 || this.leadform.customertype ==45){	
		  
  			   if(res.data[0].CompetitorData[0].iCompetitorId){
				this.leadform.competitorid_1 = res.data[0].CompetitorData[0].iCompetitorId;
				   this.competitorId_1 = res.data[0].CompetitorData[0].vCompetitor;
				   this.leadform.competitor_1 = res.data[0].CompetitorData[0].vCompetitorName;
			       this.leadform.machinetype_1 = res.data[0].CompetitorData[0].vMachineType;
				   this.leadform.quantity_1 = res.data[0].CompetitorData[0].iQuantity;
				   this.competitostatus = true;
				    this.compitiornew =1;
				}
				
				if(res.data[0].CompetitorData[1].iCompetitorId){
					this.leadform.competitorid_2 = res.data[0].CompetitorData[1].iCompetitorId;
				   this.competitorId_2 = res.data[0].CompetitorData[1].vCompetitor;
				   this.leadform.competitor_2 = res.data[0].CompetitorData[1].vCompetitorName;
			        this.leadform.machinetype_2 = res.data[0].CompetitorData[1].vMachineType;
				    this.leadform.quantity_2 = res.data[0].CompetitorData[1].iQuantity;
					this.competitostatus = true;
					this.competitostatus1 = true;
					  this.compitiornew =2;
				}
				
				
				if(res.data[0].CompetitorData[2].iCompetitorId){
					this.leadform.competitorid_3 = res.data[0].CompetitorData[2].iCompetitorId;
					this.competitorId_3 = res.data[0].CompetitorData[2].vCompetitor;
					this.leadform.competitor_3 = res.data[0].CompetitorData[2].vCompetitorName;
			        this.leadform.machinetype_3 = res.data[0].CompetitorData[2].vMachineType;
				    this.leadform.quantity_3 = res.data[0].CompetitorData[2].iQuantity;
					this.competitostatus = true;
					this.competitostatus1 = true;
					this.competitostatus2 = true;	
					  this.compitiornew =3;
				}
				
				
				if(res.data[0].CompetitorData[3].iCompetitorId){
					this.leadform.competitorid_4 = res.data[0].CompetitorData[3].iCompetitorId;
					this.competitorId_4 = res.data[0].CompetitorData[3].vCompetitor;
					this.leadform.competitor_4 = res.data[0].CompetitorData[3].vCompetitorName;
			        this.leadform.machinetype_4 = res.data[0].CompetitorData[3].vMachineType;
				    this.leadform.quantity_4 = res.data[0].CompetitorData[3].iQuantity;
					this.competitostatus = true;
					this.competitostatus1 = true;
					this.competitostatus2 = true;	
					this.competitostatus3 = true;	
					  this.compitiornew =4;
				}
				
			
				if(res.data[0].CompetitorData[4].iCompetitorId){
					this.leadform.competitorid_5 = res.data[0].CompetitorData[4].iCompetitorId;
					this.competitorId_5 = res.data[0].CompetitorData[4].vCompetitor;
					this.leadform.competitor_5 = res.data[0].CompetitorData[4].vCompetitorName;
			        this.leadform.machinetype_5 = res.data[0].CompetitorData[4].vMachineType;
				    this.leadform.quantity_5 = res.data[0].CompetitorData[4].iQuantity;
					this.competitostatus = true;
					this.competitostatus1 = true;
					this.competitostatus2 = true;	
					this.competitostatus3 = true;	
					this.competitostatus4 = true;	
					  this.compitiornew =5;
				}
		
		}else{
			this.editleadform.get('competitor_1').disable();
			this.editleadform.get('machinetype_1').disable();
			this.editleadform.get('quantity_1').disable();
		}
			  
			  
		   }
	  },
	 error =>  {this.errorMessage = <any>error;
	   this.hideLoading();
	    if(this.errorMessage.status == 401){ 
				  this.storageService.setObject("tokan", '');
				  this.navCtrl.setRoot(LoginPage);
			   }
		this.showToast(this.errorMessage.message.error);
	 });
   }
  
  
    showLoading(){
	  this.loading = this.loadingCtrl.create({
        content: 'Please wait...'
       });
	    this.loading.present();
  }
  
  hideLoading(){
	  this.loading.dismiss();
  }
  
  showToast(msg) {
	  let toast = this.toastCtrl.create({
		message: msg,
		duration: 2000,
		position: 'top'
	  });
      toast.present();
  }
  
  
   
  
  
  
  
   back(){
	 this.navCtrl.pop(); 
  }
  

}
