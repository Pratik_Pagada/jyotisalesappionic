import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController,AlertController , NavParams ,LoadingController,Content,ToastController,ModalController} from 'ionic-angular';
import {Validators, FormBuilder, FormGroup,FormControl } from '@angular/forms';
import { RestProvider } from '../../providers/rest/rest';
import { MachinemodalComponent } from '../../components/machinemodal/machinemodal';
import { StorageProvider } from '../../providers/storage/storage';
import { LoginPage } from '../login/login';
import { CompititorComponent } from '../../components/compititor/compititor';

@IonicPage()
@Component({
  selector: 'page-productdetail',
  templateUrl: 'productdetail.html',
})
export class ProductdetailPage {

	@ViewChild(Content) content: Content;

   prdctid:any;
   productform:any;
   errorMessage:any;
   loading : any;
   private editproductform : FormGroup;
       orderStatus:any;
	 comapnytypeList:any;
	 currencyList:any;
	 lostorderList:any;
	 modeofpaymentList:any;
	 orderstatusList:any;
	 probabilityList:any;
	 supporthdList:any;
	 allDropList:any;
	 controllerList:any;
	 modelList:any;
	 copititorList:any;
	 leadOBJ:any;
	 crntdate:any;
	 totalval:any;
	  modelslct:any;
	 modelid:any;
	 maxdate:any;
	 competitorLabel_1:any;
	 competitorLabel_2:any;
	 competitorId_1:any;
	 competitorId_2:any;
	 readonlysttus:any;
   
  constructor(public alertCtrl: AlertController,private storageService:StorageProvider,public navCtrl: NavController, public navParams: NavParams,public rest : RestProvider,public loadingCtrl: LoadingController,private formBuilder: FormBuilder,private toastCtrl: ToastController,public modalCtrl: ModalController) {
	  
	
	  this.prdctid =   this.navParams.get('pid');
	  this.modelslct="";
	this.productform={};
	this.readonlysttus = false;
	
	 this.editproductform = this.formBuilder.group({
		modelname: ['', Validators.required],
        controller: ['', Validators.required],
		machineval: ['', Validators.compose([Validators.required, Validators.pattern('[0-9]+$'), Validators.minLength(6)])],
		qty: ['', Validators.compose([Validators.required,  Validators.pattern('[0-9]+$'),Validators.maxLength(3)])],
		totalval: ['', Validators.required],
        probability: ['', Validators.required],
		probabilityjyoti: ['', Validators.required],
		quotationno: ['', Validators.required],
		makingdate:['', Validators.required],
		ordersatus: ['', Validators.required],
		orderfinaldate:  ['', Validators.required],
		
		finalmachinqty: ['', Validators.required],
		finalval:  ['', Validators.compose([Validators.required, Validators.minLength(6)])],
		advanceamt:  ['',Validators.compose([Validators.required, Validators.minLength(5)])],
		collectiondate:  ['', Validators.required],
		
		paymentmode:  ['', Validators.required],
		currencytype:  ['', Validators.required],
		supporthd:[''],
		bankname:  ['', Validators.required],
		chequeno: ['', Validators.required],
		paymentterm:[''],
		reason:[''],
		lostordrdue: ['', Validators.required],
		compatitor1:[''],
		compatitor2:[''],
		othermodel: ['', Validators.required],
	//	photo:[''],
		comment:[''],
		dLiftingDate:['']
     });
	
	 this.getdropdownlist();
	  this.getcontrollerlist();
	  this.getmodellist();
	  this.getcopititorList();
	  
	  let currentdate = new Date();
	  let crntDay =currentdate.getDate();
	  let crntMonth = currentdate.getMonth();
	  crntMonth = crntMonth+1;
	  let maxyear = currentdate.getFullYear();
	  maxyear = maxyear+5;
		
	if(crntDay<10 && crntMonth<10){this.maxdate = maxyear+"-0"+crntMonth+"-0"+crntDay;}
	else if (crntDay<10 && crntMonth>9){this.maxdate = maxyear+"-"+crntMonth+"-0"+crntDay;}
	else if (crntDay>9 && crntMonth<10){this.maxdate = maxyear+"-0"+crntMonth+"-"+crntDay;}
	else{this.maxdate = maxyear+"-"+crntMonth+"-"+crntDay; }
	  
  }


 

  ionViewDidLoad() {
   
	let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth()+1; //January is 0!
    let yyyy = today.getFullYear();
   
   if(dd<10 && mm<10){this.crntdate = yyyy+"-0"+mm+"-0"+dd;}
   else if (dd<10 && mm>9){this.crntdate = yyyy+"-"+mm+"-0"+dd;}
   else if (dd>9 && mm<10){this.crntdate = yyyy+"-0"+mm+"-"+dd;}
   else{this.crntdate = yyyy+"-"+mm+"-"+dd; }
   
  }
  
  
   prdctTotal(mval,qnty){
	  if(mval){
		  if(qnty){
			  this.totalval = mval*qnty;
		 }
	  }
   }
   
   
    selectmodel() {  
    if(this.readonlysttus == false){
        let data = { "title": "model", "data": this.modelList }
        let modal = this.modalCtrl.create(MachinemodalComponent, data);
          modal.onDidDismiss(data => {
             if (data) {
               this.modelslct = data.product_name;
		       this.modelid = data.id;
		     }
          });
		modal.present();
	}
   }


     
   validateAllFormFields(formGroup: FormGroup) {         //{1}
   Object.keys(formGroup.controls).forEach(field => {  //{2}
	 const control = formGroup.get(field);             //{3}
	 if (control instanceof FormControl) {             //{4}
	   control.markAsTouched({ onlySelf: true });
	 } else if (control instanceof FormGroup) {        //{5}
	   this.validateAllFormFields(control);            //{6}
	 }
   });
  }


  selectcompetitor_1() {  
	if(this.readonlysttus == false){
	  let data = { "title": "Company", "data":  this.copititorList }
	   let modal = this.modalCtrl.create(CompititorComponent, data);
	   modal.onDidDismiss(data => {
		   
		 if (data) {
		  this.competitorLabel_1 = data.vCompetitorName;
		  this.competitorId_1 = data.iCompetitorListId;
		 
		 }
	   });
	   modal.present();
	}
}

 selectcompetitor_2() {  
	if(this.readonlysttus == false){
	  let data = { "title": "Company", "data":  this.copititorList }
	   let modal = this.modalCtrl.create(CompititorComponent, data);
	   modal.onDidDismiss(data => {
		   
		 if (data) {
		  this.competitorLabel_2 = data.vCompetitorName;
		  this.competitorId_2 = data.iCompetitorListId;
		 
		 }
	   });
	   modal.present();
	}
}
	
   
     
  productformSubmit(){
	
	if (this.editproductform.valid) {
		if(this.productform.qty>0){  
			if(this.productform.advanceamt){
			if(Number(this.productform.advanceamt)<Number(this.productform.finalval)){  
				if(this.productform.finalmachinqty>0){	
					
					let confirm = this.alertCtrl.create({
						title: '',
						message: 'Congratulation for your order confirmation.please tap yes to save.',
						buttons: [
						  {
							text: 'No',
							handler: () => {
							  console.log('Disagree clicked');
							}
						  },
						  {
							text: 'Yes',
							handler: () => {
								this.showLoading();
								this.rest.editProduct(this.productform,this.prdctid,this.totalval,this.modelid,this.competitorId_1,this.competitorId_2).subscribe(data => {
								   
									 this.hideLoading();
									  if(data.status ==200){ 
										this.showToast(data.message.success)
									  }
									  this.readonlysttus =true;
								  },
								  error =>  {this.errorMessage = <any>error;
									   this.hideLoading();
										if(this.errorMessage.status == 401){ 
										 this.storageService.setObject("tokan", '');
										 this.navCtrl.setRoot(LoginPage);
									   }
									   this.showToast(this.errorMessage.message.error);
								});
							}
						  }
						]
					  });

                confirm.present();
	}else{
		alert("Minimum Final Machine Qty is required 1");
	}
	}else{
		alert("Advance Amount is not grater than final value");
	}
  }else{
	this.showLoading();
	this.rest.editProduct(this.productform,this.prdctid,this.totalval,this.modelid,this.competitorId_1,this.competitorId_2).subscribe(data => {
	   
		 this.hideLoading();
		  if(data.status ==200){ 
			this.showToast(data.message.success)
		  }
	  },
	  error =>  {this.errorMessage = <any>error;
		   this.hideLoading();
			if(this.errorMessage.status == 401){ 
			 this.storageService.setObject("tokan", '');
			 this.navCtrl.setRoot(LoginPage);
		   }
		   this.showToast(this.errorMessage.message.error);
	});

  }
	}else{
		alert("Minimum Quantity is required 1");
	}

	} else {
		this.validateAllFormFields(this.editproductform); //{7}
		this.content.scrollToTop();
	  }
	  
  }
   
   
  
  getPrdctdetail(){

	  
    this.rest.productDetail(this.prdctid).subscribe(
	   res => { 
	    if(res.status ==200){  
			
                
			 this.productform.lead_id =  res.data[0].lead_id;
			  this.modelslct  =  res.data[0].product_name;
			  this.modelid =  res.data[0].model_id;
			  this.productform.controller =  res.data[0].controller_id;
			   this.productform.machineval =  res.data[0].value_per_machine;
			    this.productform.qty =  res.data[0].quantity;
				 this.totalval =  res.data[0].total_value;

				  this.productform.probability =  res.data[0].poc_id;
				  this.productform.probabilityjyoti =  res.data[0].iJyotiPur;
				   this.productform.quotationno =  res.data[0].q_number;
				    
					this.productform.makingdate =  res.data[0].dm_within;
					this.productform.ordersatus =  res.data[0].order_status;
					this.productform.orderfinaldate =  res.data[0].order_final_date;
				
					this.productform.finalmachinqty =  res.data[0].final_machine_quantity;
					this.productform.collectiondate =  res.data[0].date_of_advance_collection;
					
					this.productform.paymentmode =  res.data[0].mode_of_payment;
					
					
					this.productform.currencytype =  res.data[0].currency;
					this.productform.supporthd =  res.data[0].support_for_ho;
					this.productform.bankname =  res.data[0].bank_name;
					this.productform.chequeno =  res.data[0].cdtr_no;
					this.productform.paymentterm =  res.data[0].payment_terms;
					if(res.data[0].reason){
						this.productform.reason =  res.data[0].reason;
					}else{
						this.productform.reason =  "";
					}
					
					this.productform.lostordrdue =  res.data[0].lost_order_due_to;
				
					this.competitorId_1 =  res.data[0].competitor_1;
					this.competitorLabel_1 =  res.data[0].competitor_1Name;

			
					 this.competitorId_2 =  res.data[0].competitor_2;
					 this.competitorLabel_2 =  res.data[0].competitor_2Name;
					 

			     	this.productform.othermodel =  res.data[0].other_model_id;
					this.productform.comment =  res.data[0].comment;
					this.productform.dLiftingDate =  res.data[0].dLiftingDate;
					
					this.productform.finalval =  res.data[0].final_value;
					this.productform.advanceamt =  res.data[0].final_amount_value;
					
					
			      this.productform.supporthd =  this.productform.supporthd.split(",");
				 this.changeorderstatus(this.productform.ordersatus)
				 if(this.productform.ordersatus == "61"){
                    this.readonlysttus = true;
				 }

		   }
	     this.hideLoading();
	   },
	 error =>  {this.errorMessage = <any>error;
	     this.hideLoading();
		  if(this.errorMessage.status == 401){ 
	          this.storageService.setObject("tokan", '');
			  this.navCtrl.setRoot(LoginPage);
	        }
			this.showToast(this.errorMessage.message.error);
	 });
	  
  }
  
    getdropdownlist(){
	   this.showLoading();
	 this.rest.getproductalldropdown().subscribe(data => {
		   this.allDropList = data;
	     
		   if(this.allDropList.status ==200){ 
			 for(let d =0; d<this.allDropList.data.length; d++){
				 if(this.allDropList.data[d].id==18){
					 this.currencyList = this.allDropList.data[d].optionvalues;
				 }else if(this.allDropList.data[d].id==16){
					 this.lostorderList = this.allDropList.data[d].optionvalues;
				 }else if(this.allDropList.data[d].id==17){
					 this.modeofpaymentList = this.allDropList.data[d].optionvalues;
				 }else if(this.allDropList.data[d].id==15){
					 this.orderstatusList = this.allDropList.data[d].optionvalues;
				 }else if(this.allDropList.data[d].id==13){
					 this.probabilityList = this.allDropList.data[d].optionvalues;
				 }else if(this.allDropList.data[d].id==14){
					 this.supporthdList = this.allDropList.data[d].optionvalues;
				 }
			 }
		   }
	   },
	 error =>  {this.errorMessage = <any>error;
	       
			this.showToast(this.errorMessage.message.error);
			
	 });
	 
	  
  }
  
  
  getcontrollerlist(){
	  
	 this.rest.getcontrollerdropdown().subscribe(data => {
		   this.allDropList = data;
	      if(this.allDropList.status ==200){ 
			 this.controllerList = this.allDropList.data;
		   }
	   },
	 error =>  {this.errorMessage = <any>error;
	     	this.showToast(this.errorMessage.message.error);
	 });
 }
  
  
   getmodellist(){
	 
	 this.rest.getmodeldropdown().subscribe(data => {
		   this.allDropList = data;
	       if(this.allDropList.status ==200){ 
			 this.modelList = this.allDropList.data;
		   }
	   },
	 error =>  {this.errorMessage = <any>error;
	       
			this.showToast(this.errorMessage.message.error);
			
	 });
	 
	  
  }
  
  
  getcopititorList(){ 
  
     this.rest.getcopititorList().subscribe(data => {
		 
		   this.allDropList = data;
	       if(this.allDropList.status ==200){ 
			  this.copititorList =  this.allDropList.data;
		   }
		   this.getPrdctdetail();
	   },
	   error =>  {this.errorMessage = <any>error;
	      
			this.showToast(this.errorMessage.message.error);
	 });
	 
  }
  
  changeorderstatus(ordrsts){
	if(ordrsts=="61"){
		 this.orderStatus="won";
		 this.editproductform.get('orderfinaldate').enable();
		 this.editproductform.get('finalmachinqty').enable();
		 this.editproductform.get('finalval').enable();
		 this.editproductform.get('advanceamt').enable();
		 this.editproductform.get('collectiondate').enable();
		 this.editproductform.get('paymentmode').enable();
		 this.editproductform.get('currencytype').enable();
		 this.editproductform.get('bankname').enable();
		 this.editproductform.get('chequeno').enable();
		 
		 this.editproductform.get('othermodel').disable();
		 this.editproductform.get('lostordrdue').disable();

	   
	}else if(ordrsts=="62"){
	   this.orderStatus="lost";
	   this.editproductform.get('lostordrdue').enable();

	   this.editproductform.get('orderfinaldate').disable();
	   this.editproductform.get('finalmachinqty').disable();
	   this.editproductform.get('finalval').disable();
	   this.editproductform.get('advanceamt').disable();
	   this.editproductform.get('collectiondate').disable();
	   this.editproductform.get('paymentmode').disable();
	   this.editproductform.get('currencytype').disable();
	   this.editproductform.get('bankname').disable();
	   this.editproductform.get('chequeno').disable();
	   this.editproductform.get('othermodel').disable();

	}else if(ordrsts=="64"){
	   this.orderStatus="other";
	   this.editproductform.get('othermodel').enable();

	   this.editproductform.get('lostordrdue').disable();
	   this.editproductform.get('orderfinaldate').disable();
	  this.editproductform.get('finalmachinqty').disable();
	  this.editproductform.get('finalval').disable();
	  this.editproductform.get('advanceamt').disable();
	  this.editproductform.get('collectiondate').disable();
	  this.editproductform.get('paymentmode').disable();
	  this.editproductform.get('currencytype').disable();
	  this.editproductform.get('bankname').disable();
	  this.editproductform.get('chequeno').disable();

	}else if(ordrsts=="65"){
	   this.orderStatus="drop";
	   this.editproductform.get('othermodel').disable();
	   
	  this.editproductform.get('lostordrdue').disable();
	  this.editproductform.get('orderfinaldate').disable();
	  this.editproductform.get('finalmachinqty').disable();
	  this.editproductform.get('finalval').disable();
	  this.editproductform.get('advanceamt').disable();
	  this.editproductform.get('collectiondate').disable();
	  this.editproductform.get('paymentmode').disable();
	  this.editproductform.get('currencytype').disable();
	  this.editproductform.get('bankname').disable();
	  this.editproductform.get('chequeno').disable();
	}
	else if(ordrsts=="63"){
	   this.orderStatus="pending";

	   this.editproductform.get('othermodel').disable();
	   
	  this.editproductform.get('lostordrdue').disable();
	  this.editproductform.get('orderfinaldate').disable();
	  this.editproductform.get('finalmachinqty').disable();
	  this.editproductform.get('finalval').disable();
	  this.editproductform.get('advanceamt').disable();
	  this.editproductform.get('collectiondate').disable();
	  this.editproductform.get('paymentmode').disable();
	  this.editproductform.get('currencytype').disable();
	  this.editproductform.get('bankname').disable();
	  this.editproductform.get('chequeno').disable();
	}
   
}


changepaymentmode(paymentmode){
	if(paymentmode == 77){
		this.editproductform.get('bankname').disable();
		this.editproductform.get('chequeno').disable();
	}else{
		this.editproductform.get('bankname').enable();
		this.editproductform.get('chequeno').enable();
	}

}
  
  
   showLoading(){
	  this.loading = this.loadingCtrl.create({
        content: 'Please wait...'
       });
	    this.loading.present();
  }
  
  hideLoading(){
	  this.loading.dismiss();
  }
  
  showToast(msg) {
	  let toast = this.toastCtrl.create({
		message: msg,
		duration: 3000,
		position: 'top'
	  });
      toast.present();
  }
  
  
  
   back(){
	 this.navCtrl.pop(); 
  }

}
