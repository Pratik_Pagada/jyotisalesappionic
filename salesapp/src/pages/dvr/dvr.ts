import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform,ToastController,LoadingController} from 'ionic-angular';
// import { AdddvrPage } from '../adddvr/adddvr';
import { RestProvider } from '../../providers/rest/rest';
import { DvrdetailPage } from '../dvrdetail/dvrdetail';
import { Diagnostic } from '@ionic-native/diagnostic';
import { StorageProvider } from '../../providers/storage/storage';
import { LoginPage } from '../login/login';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { LocationProvider } from '../../providers/location/location';
import { Geolocation } from '@ionic-native/geolocation';


/**
 * Generated class for the DvrPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-dvr',
  templateUrl: 'dvr.html',
})
export class DvrPage {
  errorMessage:any;
  dvrlist:any;
  locationObj:any;
  loading:any;

  constructor(private storageService:StorageProvider,public navCtrl: NavController, public plt: Platform,public navParams: NavParams,public rest : RestProvider,public loadingCtrl: LoadingController,private diagnostic: Diagnostic,private toastCtrl: ToastController,private geolocation: Geolocation,private locationService:LocationProvider,private sqlite: SQLite) {
    this.locationObj ={};
	this.getDvrList();
	  this.checklocationservice();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DvrPage');
	
  }
  
  back(){
	 this.navCtrl.pop(); 
  }
  
  
  
  
    checklocationservice(){
      if (this.plt.is('ios')) {
        this.diagnostic.isLocationEnabled().then((isAvailable) => {
          
         if (isAvailable){
            
         } else {
           this.diagnostic.switchToLocationSettings();
         }
         }).catch(e => alert(e));
      }else{
        this.diagnostic.isGpsLocationEnabled().then((isAvailable) => {
          
         if (isAvailable){
            
         } else {
           this.diagnostic.switchToLocationSettings();
         }
         }).catch(e => alert(e));

      }

  
    }
  
  
  
  
  
  
  getDvrList() {
    this.showLoading();
	 
    this.rest.getDvrList().subscribe(
	   res => {
	    if(res.status ==200){ 
			  this.dvrlist =  res.data;
		   }
	     this.hideLoading();
	   },
	 error =>  {this.errorMessage = <any>error;
	    if(this.errorMessage.status == 401){ 
	          //alert(this.errorMessage.message.error);
		      this.storageService.setObject("tokan", '');
			  this.navCtrl.setRoot(LoginPage);
	   }
     this.hideLoading();
	 });
  }

  dvrdetail(did) {
   this.navCtrl.push(DvrdetailPage,{'did':did}); 
   
  }
  
  

  Adddvr(){
    // this.navCtrl.push(AdddvrPage); 
    
    this.plt.ready().then(() => {
      
          //	this.storageService.getObject("dvrid").then((val)=>{
      
              
              
      
                let opt ={maximumAge:9000,timeout:9000,enableHighAccuracy:true};   
                
                  this.geolocation.getCurrentPosition(opt).then((resp) => {
                    
                    this.locationObj.lat = resp.coords.latitude;
                    this.locationObj.long = resp.coords.longitude;
                    
                    
                    this.showLoading();
                     this.rest.startdvrSubmt(this.locationObj.lat,this.locationObj.long).subscribe(data => {
                       this.hideLoading();
                       if(data.status ==200){ 
                       
                         let currentdate = new Date();
                        let crntMonth = currentdate.getMonth();
                        crntMonth = crntMonth+1;
                        
                        let datetime =  currentdate.getDate() + "/"+ crntMonth + "/" + currentdate.getFullYear() + " " + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
      
      
                        this.sqlite.create({
                          name: 'salesdb.db',
                          location: 'default'
                          }).then((db: SQLiteObject) => {
                          db.executeSql('INSERT INTO locationdata VALUES(NULL,?,?,?)',[this.locationObj.lat,this.locationObj.long,datetime])
                            .then(res => {
                            console.log(res);
                            
                            })
                            .catch(e => {
                            console.log(e);
                             
                            });
                          }).catch(e => {
                          console.log(e);
                           
                          });
      
                       
                          this.showToast(data.message.success);
                          this.storageService.setObject("dvrid", data.data.iDvrId);
                          this.locationService.startTracking();
                          this.getDvrList();
                          
                         }
                       },
                     error =>  {this.errorMessage = <any>error;
                       if(this.errorMessage.status == 401){ 
                                          this.storageService.setObject("tokan", '');
                          this.navCtrl.setRoot(LoginPage);
                       }
                        this.hideLoading();
                        this.showToast(this.errorMessage.message.error);
                     });
                    
                    
                    
                     
                    }).catch((error) => {
                      this.hideLoading();
                      this.Adddvr();
                    // alert('Error getting location');
                    });
              
            // }); 
             
           
      
          });


  }  
  


  showLoading(){
	  this.loading = this.loadingCtrl.create({
        content: 'Please wait...'
       });
	    this.loading.present();
  }
  
  hideLoading(){
	  this.loading.dismiss();
  }
  
  showToast(msg) {
	  let toast = this.toastCtrl.create({
		message: msg,
		duration: 3000,
		position: 'top'
	  });
      toast.present();
  }



}
