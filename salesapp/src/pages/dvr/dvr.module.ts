import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DvrPage } from './dvr';

@NgModule({
  declarations: [
    DvrPage,
  ],
  imports: [
    IonicPageModule.forChild(DvrPage),
  ],
  exports: [
    DvrPage
  ]
})
export class DvrPageModule {}
