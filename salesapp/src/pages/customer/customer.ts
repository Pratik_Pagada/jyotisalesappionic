import { Component ,ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams ,LoadingController,Content,ToastController,ModalController} from 'ionic-angular';
import {Validators, FormBuilder, FormGroup,FormControl } from '@angular/forms';
import { RestProvider } from '../../providers/rest/rest';
import { StatemodalComponent } from '../../components/statemodal/statemodal';
import { StorageProvider } from '../../providers/storage/storage';
import { LoginPage } from '../login/login';

/**
 * Generated class for the CustomerPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-customer',
  templateUrl: 'customer.html',
})
export class CustomerPage {
   @ViewChild(Content) content: Content;
    
     private customerform : FormGroup;
	  errorMessage:any;
	 loading : any;
	 allDropList:any;
	 countryList:any;
	 comapnystatusList:any;
	 comapnytypeList:any;
	 IndustryList:any;
	 stateList:any;
	 exportstatus:any;
	 stateslct:any;
	 stateslctid:any;
	 countrySlct:any;
	 
  constructor(private storageService:StorageProvider,public navCtrl: NavController, public navParams: NavParams,private formBuilder: FormBuilder, public rest : RestProvider,public loadingCtrl: LoadingController,private toastCtrl: ToastController,public modalCtrl: ModalController) {
	  
	  this.customerform = this.formBuilder.group({
	    comapnyname: ['', Validators.required],
        firstname: ['',  Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z ]*')])],
		comapnyref: [''],
		comapnytype: ['', Validators.required],
		comapnystatus: ['', Validators.required],
        industrytype: ['', Validators.required],
		email: ['', Validators.compose([Validators.required,Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')])],
		country: ['', Validators.required],
		state: ['', Validators.required],
		city:['', Validators.required],
		address:['', Validators.required],
		exportstate: [''],
		exportcity: [''],
		postalcode: [''],
		primaryphone: ['', Validators.required],
		secondaryphone: [''],
		website: ['']
		
     });
	 
	 this.getdropdownlist();
	 this.getcountrydownlist();
	 this.exportstatus = true;
  }
  
  back(){
	 this.navCtrl.pop(); 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CustomerPage');
  }
  
  getdropdownlist(){
	   this.showLoading();
	 this.rest.getcusdropdown().subscribe(data => {
		   this.allDropList = data;
	     
		   if(this.allDropList.status ==200){ 
			 for(let d =0; d<this.allDropList.data.length; d++){
				 if(this.allDropList.data[d].id==22){
					 this.comapnystatusList = this.allDropList.data[d].optionvalues;
				 }else if(this.allDropList.data[d].id==21){
					 this.comapnytypeList = this.allDropList.data[d].optionvalues;
				 }else if(this.allDropList.data[d].id==23){
					 this.IndustryList = this.allDropList.data[d].optionvalues;
				 }
			 }
		   }
	   },
	 error =>  {this.errorMessage = <any>error;
	       
			this.showToast(this.errorMessage.message.error);
			
	 });
	  
  }
  
    selectstate() {  
   
       let data = { "title": "state", "data": this.stateList }
        let modal = this.modalCtrl.create(StatemodalComponent, data);
        modal.onDidDismiss(data => {
			
          if (data) {
           this.stateslct = data.StateName;
		   this.stateslctid = data.id;
		 
          }
        });
        modal.present();
   }

  
  
  getcountrydownlist(){
	   
	 this.rest.getcountrydropdown().subscribe(data => {
		   this.hideLoading();
		   if(data.status ==200){ 
			 this.countryList = data.data;
			 this.countrySlct =101;
			 this.changecontry(101);
			 
		   }
		 },
	   error =>  {this.errorMessage = <any>error;
	        this.hideLoading();
			this.showToast(this.errorMessage.message.error);
	 });
	  
  }
  
 
  
   getstatedroplist(cid){
	 this.showLoading();
	 this.rest.getstatelist(cid).subscribe(data => {
		   this.hideLoading();
		   if(data.status ==200){ 
			 this.stateList = data.data;
		   }
	   },
	   error =>  {this.errorMessage = <any>error;
	        this.hideLoading();
			this.showToast(this.errorMessage.message.error);
	 });
	} 
  
  changecontry(cid){ 
	   if(cid){ 
	     if(cid==101){this.exportstatus=false;}else{this.exportstatus=true;}
	      this.getstatedroplist(cid);
	   }
  }
  
  
  validateAllFormFields(formGroup: FormGroup) {         //{1}
 Object.keys(formGroup.controls).forEach(field => {  //{2}
   const control = formGroup.get(field);             //{3}
   if (control instanceof FormControl) {             //{4}
	 control.markAsTouched({ onlySelf: true });
   } else if (control instanceof FormGroup) {        //{5}
	 this.validateAllFormFields(control);            //{6}
   }
 });
}
  
  customerformSubmit(){
	  if (this.customerform.valid) {
	  this.showLoading();
	 this.rest.addCustomer(this.customerform.value,this.stateslctid,this.countrySlct).subscribe(data => {
		   this.hideLoading();
		   if(data.status ==200){ 
		   this.customerform.reset();
			 this.showToast(data.message.success);
		   }
	   },
	   error =>  {this.errorMessage = <any>error;
	        this.hideLoading();
			if(this.errorMessage.status == 401){ 
	          this.storageService.setObject("tokan", '');
			  this.navCtrl.setRoot(LoginPage);
	        }
			this.showToast(this.errorMessage.message.error);
	 });
	  
	   } else {
		this.validateAllFormFields(this.customerform); //{7}
		this.content.scrollToTop();
	  }
  }
  
  
   showLoading(){
	  this.loading = this.loadingCtrl.create({
        content: 'Please wait...'
       });
	    this.loading.present();
  }
  
  hideLoading(){
	  this.loading.dismiss();
  }
  
  showToast(msg) {
	  let toast = this.toastCtrl.create({
		message: msg,
		duration: 3000,
		position: 'top'
	  });
      toast.present();
  }
  
  
}
