import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { ProductdetailPage } from '../productdetail/productdetail';
import { StorageProvider } from '../../providers/storage/storage';
import { LoginPage } from '../login/login';
/**
 * Generated class for the ProductPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
})
export class ProductPage {
	
  countries: string[];
  errorMessage: any;
  selectedItem: any;
  pedctlist:any;
  
  constructor(private storageService:StorageProvider,public navCtrl: NavController, public navParams: NavParams,public rest : RestProvider,public loadingCtrl: LoadingController) {
   
   this.getproductlist();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductPage');
  }
  
  getproductlist() {
	  let loading = this.loadingCtrl.create({
        content: 'Please wait...'
       });

     loading.present();
	 
    this.rest.getProductList().subscribe(
	   res => { 
	    if(res.status ==200){ 
			  this.pedctlist =  res.data;
		   }
	   loading.dismiss();
	   },
	 error =>  {this.errorMessage = <any>error;
	  
	   loading.dismiss();
	   if(this.errorMessage.status == 401){ 
	          //alert(this.errorMessage.message.error);
		      this.storageService.setObject("tokan", '');
			  this.navCtrl.setRoot(LoginPage);
	   }
	 });
  }

  prdctdetail(pid) {
     this.navCtrl.push(ProductdetailPage,{'pid':pid}); 
   
   }
  
   back(){
	 this.navCtrl.pop(); 
  }

}
