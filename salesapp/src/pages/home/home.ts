import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { CustomerPage } from '../customer/customer';
import { BranchPage } from '../branch/branch';
import { HelpPage } from '../help/help';
import { ProductPage } from '../product/product';
import { ListPage } from '../list/list';
import { NewleadPage } from '../newlead/newlead';
import { DvrPage } from '../dvr/dvr';
import { RestProvider } from '../../providers/rest/rest';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public rest : RestProvider) {
     this.rest.getTokan();
  }
  
  addCustomer(){
	 this.navCtrl.push(CustomerPage);
  }
  
  newlead(){
	  this.navCtrl.push(NewleadPage);
 }
  
  leadlist(){
	  this.navCtrl.push(ListPage);
  }
  
  product(){
	  this.navCtrl.push(ProductPage);
 }
  
  help(){
	  this.navCtrl.push(DvrPage);
 }
  
  branch(){
	 this.navCtrl.push(BranchPage); 
  }
  
}
