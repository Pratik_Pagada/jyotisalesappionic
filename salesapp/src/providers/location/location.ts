import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

/*
  Generated class for the LocationProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class LocationProvider {

  public lat: number = 0;
  public lng: number = 0;

  constructor(public http: Http,private backgroundGeolocation: BackgroundGeolocation,private sqlite: SQLite) {
   
  }


  startTracking() {

            const config: BackgroundGeolocationConfig = {
              desiredAccuracy: 10,
              stationaryRadius: 30,
              distanceFilter: 30,
              debug: false, //  enable this hear sounds for background-geolocation life-cycle.
              stopOnTerminate: false, // enable this to clear background location settings when the app terminates
              pauseLocationUpdates:false,
              interval: 10000,
              fastestInterval: 5000,
              startOnBoot:true,
              activitiesInterval: 10000,
              notificationTitle:"Sales App",
              notificationText:"Background Geolocation",
              activityType:"AutomotiveNavigation"

             

            };

          this.backgroundGeolocation.configure(config)
          .subscribe((location: BackgroundGeolocationResponse) => {
            // alert(JSON.stringify(location))
            
              // alert('BackgroundGeolocation:  ' + location.serviceProvider + ',' + location.longitude);
                this.backgroundGeolocation.finish(); // FOR IOS ONLY

              // if(location.provider=="gps"){
                   this.AddLocation(location.latitude,location.longitude,location)
             //  }
          });

          // start recording location
          this.backgroundGeolocation.start();
     }


     stopTracking() {
      
      this.backgroundGeolocation.stop();

     }


   AddLocation(alat,along,locobj){
    if(locobj.provider=="gps"){
		
		let currentdate = new Date();
		let crntMonth = currentdate.getMonth();
		crntMonth = crntMonth+1;
		
		let datetime =  currentdate.getDate() + "/"+ crntMonth + "/" + currentdate.getFullYear() + " " + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
     
    this.sqlite.create({
      name: 'salesdb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('INSERT INTO locationdata VALUES(NULL,?,?,?)',[alat,along,datetime])
        .then(res => {
          console.log(res);
          
        })
        .catch(e => {
          console.log(e);
         
        });
    }).catch(e => {
      console.log(e);
     
    });

   }

  }









}
