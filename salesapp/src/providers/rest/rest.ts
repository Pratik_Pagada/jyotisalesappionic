
import { Injectable  } from '@angular/core';
import { Http, Response ,Headers,RequestOptions} from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { StorageProvider } from '../storage/storage';
import { Storage } from '@ionic/storage';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class RestProvider {

	

   public baseurl:string ='http://finix.jyoti.co.in/'
    
   public tokan : string;
   
  constructor(public http: Http,private storageService:StorageProvider,public storage: Storage) {
          this.storageService.getObject("tokan").then((val)=>{
			  this.tokan = val;
		  }); 
		  
		 

		 
  }
  
  
  getTokan() {
     this.storageService.getObject("tokan").then((val)=>{
			  this.tokan = val;
	 }); 
  }
  


  
  
  getproduct(): Observable<string[]> {
    return this.http.get('http://jyoti.co.in/webservice/report-a-problem.php').map(this.extractData).catch(this.handleError);
  }
  
   getdemoestic(): Observable<string[]> {
    return this.http.get('http://jyoti.co.in/webservice/domestic-sales-service.php').map(this.extractData).catch(this.handleError);
  }
  
   getinternational(): Observable<string[]> {
    return this.http.get('http://jyoti.co.in/webservice/internation-sales-service.php').map(this.extractData).catch(this.handleError);
  }
  
  
  
  getcusdropdown(): Observable<any> {
	 return this.http.get(this.baseurl+'sales/salesapi/ws/v1/general/alldropdownlist').map(this.extractData).catch(this.handleError);
  } 
  
    getleaddropdown(): Observable<any> {
	 return this.http.get(this.baseurl+'sales/salesapi/ws/v1/general/alldropdownlistlead').map(this.extractData).catch(this.handleError);
  } 
   getcustomerList(): Observable<any> {
	   let headers = new Headers();
       headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
	   headers.append('Vauthtoken','Bearer '+this.tokan);
       let reqOptions = new RequestOptions({ headers: headers });
	 return this.http.get(this.baseurl+'sales/salesapi/ws/v1/leads/customerdropdownlist',reqOptions).map(this.extractData).catch(this.handleError);
  } 
  
  
   getleadList(): Observable<any> {
	   let headers = new Headers();
       headers.append('Content-Type', 'application/x-www-form-urlencoded;  charset=UTF-8');
	   headers.append('Vauthtoken','Bearer '+this.tokan);
       let reqOptions = new RequestOptions({ headers: headers });
	   
	   
	   
	 return this.http.get(this.baseurl+'sales/salesapi/ws/v1/leads/leadlist',reqOptions).map(this.extractData).catch(this.handleError);
  } 

  getleadList1(): Observable<any> {
	let headers = new Headers();
	headers.append('Content-Type', 'application/x-www-form-urlencoded;  charset=UTF-8');
	headers.append('Vauthtoken','Bearer '+this.tokan);
	let reqOptions = new RequestOptions({ headers: headers });
	
	
	
  return this.http.get(this.baseurl+'sales/salesapi/ws/v1/leads/leadlistdd',reqOptions).map(this.extractData).catch(this.handleError);
} 
  
  

  
  
   getcopititorList(): Observable<any> {
	 return this.http.get(this.baseurl+'sales/salesapi/ws/v1/general/competitorlist').map(this.extractData).catch(this.handleError);
  } 
  

  getcountrydropdown(): Observable<any> {
	 return this.http.get(this.baseurl+'sales/salesapi/ws/v1/general/countylist').map(this.extractData).catch(this.handleError);
  }   
  
  
  getproductalldropdown(): Observable<any> {
	 return this.http.get(this.baseurl+'sales/salesapi/ws/v1/general/alldropdownlistproduct').map(this.extractData).catch(this.handleError);
  }  
  
   getcontrollerdropdown(): Observable<any> {
	 return this.http.get(this.baseurl+'sales/salesapi/ws/v1/general/controllerlist').map(this.extractData).catch(this.handleError);
  } 
 
  getmodeldropdown(): Observable<any> {
	 return this.http.get(this.baseurl+'sales/salesapi/ws/v1/general/modelddlist').map(this.extractData).catch(this.handleError);
  } 
  
  getdvrdroplist(): Observable<any> {
	 return this.http.get(this.baseurl+'sales/salesapi/ws/v1/general/alldropdownlistdvr').map(this.extractData).catch(this.handleError);
  } 
  
  
 
 
 
  getstatelist(countryid): Observable<any> {
	   let headers = new Headers();
       headers.append('Content-Type', 'application/json');
     
       let link = this.baseurl+'sales/salesapi/ws/v1/general/statelist';
	   let data = new FormData();
       data.append("country_id", countryid);
	   
	   return   this.http.post(link, data,headers).map(this.extractData).catch(this.handleError);
  } 
  
   getProductList(): Observable<any> {
	   let headers = new Headers();
    //   headers.append('Content-Type', 'application/x-www-form-urlencoded;  charset=UTF-8');
	   headers.append('Vauthtoken','Bearer '+this.tokan);
       let reqOptions = new RequestOptions({ headers: headers });
	   
	   
	   
	 return this.http.get(this.baseurl+'sales/salesapi/ws/v1/leads/productlist',reqOptions).map(this.extractData).catch(this.handleError);
  } 
  
  
   getDvrList(): Observable<any> {
	   let headers = new Headers();
    //   headers.append('Content-Type', 'application/x-www-form-urlencoded;  charset=UTF-8');
	   headers.append('Vauthtoken','Bearer '+this.tokan);
       let reqOptions = new RequestOptions({ headers: headers });
	   
	   
	   
	 return this.http.get(this.baseurl+'sales/salesapi/ws/v1/leads/dvrlist',reqOptions).map(this.extractData).catch(this.handleError);
  } 
  
  
  
  
  login(loginObj,platforms): Observable<any> {
	   let headers = new Headers();
       headers.append('Content-Type', 'application/json');
     
       let link = this.baseurl+'sales/salesapi/ws/v1/engineer/login';
	   let data = new FormData();

	    data.append("employee_code", loginObj.name);
		data.append("password", loginObj.pswd);
		data.append("eDeviceType",platforms);
		data.append("vPushToken", 'test');
				
        return   this.http.post(link, data,headers).map(this.extractData).catch(this.handleError);
  } 
  

  logout(): Observable<any> {
	let headers = new Headers();
	
	headers.append('Vauthtoken','Bearer '+this.tokan);
	let reqOptions = new RequestOptions({ headers: headers });

	return this.http.get(this.baseurl+'sales/salesapi/ws/v1/engineer/logout',reqOptions).map(this.extractData).catch(this.handleError);		 

} 


getprofileDetail(): Observable<any> {
	let headers = new Headers();
	
	headers.append('Vauthtoken','Bearer '+this.tokan);
	let reqOptions = new RequestOptions({ headers: headers });

	return this.http.get(this.baseurl+'sales/salesapi/ws/v1/engineer/userdetail',reqOptions).map(this.extractData).catch(this.handleError);		 

} 


  
   leadDetail(lid): Observable<any> {
	   let headers = new Headers();
  
	   headers.append('Vauthtoken','Bearer '+this.tokan);
       let reqOptions = new RequestOptions({ headers: headers });
       let link = this.baseurl+'sales/salesapi/ws/v1/leads/leaddetails';
	  
       let data = new FormData();
	   data.append("iLeadId", lid);
		
	   return   this.http.post(link, data,reqOptions).map(this.extractData).catch(this.handleError);
  } 
   
   
   customerDetail(cid): Observable<any> {
	   let headers = new Headers();
  
	   headers.append('Vauthtoken','Bearer '+this.tokan);
       let reqOptions = new RequestOptions({ headers: headers });
       let link = this.baseurl+'sales/salesapi/ws/v1/leads/customerdropdownlistdetails';
	  
       let data = new FormData();
	   data.append("id", cid);
		
	   return   this.http.post(link, data,reqOptions).map(this.extractData).catch(this.handleError);
  } 
  
  pinlocationSubmt(dvrid,lat,long,dpinno){
	   let headers = new Headers();
  
	   headers.append('Vauthtoken','Bearer '+this.tokan);
       let reqOptions = new RequestOptions({ headers: headers });
       let link = this.baseurl+'sales/salesapi/ws/v1/leads/pinlocation';
	   
	  
       let data = new FormData();
	   data.append("iDvrId", dvrid);
	    data.append("vLatitude", lat);
		 data.append("vLongitude", long);
		 data.append("iLocationNo", dpinno);
		 
		//  data.append("vCustomerLocation", customerlocation);
		//   data.append("iVisitType", visittype);
		//    data.append("vPurpose", purpose);
		
	   return   this.http.post(link, data,reqOptions).map(this.extractData).catch(this.handleError);
	  
  }
  
  
  startdvrSubmt(lat,long): Observable<any> {
	   let headers = new Headers();
  
	   headers.append('Vauthtoken','Bearer '+this.tokan);
       let reqOptions = new RequestOptions({ headers: headers });
       let link = this.baseurl+'sales/salesapi/ws/v1/leads/startdvr';
	  
       let data = new FormData();
	   data.append("vStartLat", lat);
	    data.append("vStartLong", long);
	
	   return   this.http.post(link, data,reqOptions).map(this.extractData).catch(this.handleError);
  } 
  
   getlocationlist(did): Observable<any> {
	   let headers = new Headers();
  
	   headers.append('Vauthtoken','Bearer '+this.tokan);
       let reqOptions = new RequestOptions({ headers: headers });
       let link = this.baseurl+'sales/salesapi/ws/v1/leads/locationlist';
	  
       let data = new FormData();
	   data.append("iDvrId", did);
	   
	   return   this.http.post(link, data,reqOptions).map(this.extractData).catch(this.handleError);
  } 
  
  
    dvrpinlocadd(dvrpinobj,did): Observable<any> {
	   let headers = new Headers();
     
	   headers.append('Vauthtoken','Bearer '+this.tokan);
       let reqOptions = new RequestOptions({ headers: headers });
       let link = this.baseurl+'sales/salesapi/ws/v1/leads/updatelocation';
	  
       let data = new FormData();
	   data.append("iLocationId", dvrpinobj.iLocationId);
	   // data.append("vCustomerLocation", dvrpinobj.location);
		 data.append("vPurpose", dvrpinobj.purpose);
		  data.append("iVisitType", dvrpinobj.visittype);
		   data.append("iCustomerId", dvrpinobj.leadid);
		  
		
	   return   this.http.post(link, data,reqOptions).map(this.extractData).catch(this.handleError);
  } 
  
  
  enddvrSubmt(lat,long,did,endObj,locarray): Observable<any> {
	   let headers = new Headers();
  
	   headers.append('Vauthtoken','Bearer '+this.tokan);
       let reqOptions = new RequestOptions({ headers: headers });
       let link = this.baseurl+'sales/salesapi/ws/v1/leads/enddvr';
	  
  /*   let data = new FormData();
	   data.append("vEndLat", lat);
	    data.append("vEndLong", long);
		 data.append("iDvrId", did);
		  data.append("iTransportType", endObj.vehicletype);
		   data.append("iFuelType", endObj.fuieltype);
		   data.append("vAmount", endObj.priceltr);
			 data.append("vFoodAllowance", endObj.foodAllowance);
			 data.append("locationArray", locarray);
		*/
	 

		let postParams = {
			iDvrId:did,
			vEndLat:lat,
			vEndLong:long,
			iTransportType:endObj.vehicletype, 
			iFuelType:endObj.fuieltype,
			vAmount:endObj.priceltr,
			vFoodAllowance:endObj.foodAllowance,
			locationArray:locarray
		  }

		

	   return   this.http.post(link, postParams,reqOptions).map(this.extractData).catch(this.handleError);
  } 
  
   
    productDetail(pid): Observable<any> {
	   let headers = new Headers();
  
	   headers.append('Vauthtoken','Bearer '+this.tokan);
       let reqOptions = new RequestOptions({ headers: headers });
       let link = this.baseurl+'sales/salesapi/ws/v1/leads/productdetails';
	  
       let data = new FormData();
	   data.append("productid", pid);
		
	   return   this.http.post(link, data,reqOptions).map(this.extractData).catch(this.handleError);
  } 
  
  dvrDetail(did): Observable<any> {
	   let headers = new Headers();
  
	   headers.append('Vauthtoken','Bearer '+this.tokan);
       let reqOptions = new RequestOptions({ headers: headers });
       let link = this.baseurl+'sales/salesapi/ws/v1/leads/dvrdetails';
	  
       let data = new FormData();
	   data.append("iDvrId", did);
		
	   return   this.http.post(link, data,reqOptions).map(this.extractData).catch(this.handleError);
  } 
  
  
  deletecompititor(cid): Observable<any> {
	   let headers = new Headers();
  
	   headers.append('Vauthtoken','Bearer '+this.tokan);
       let reqOptions = new RequestOptions({ headers: headers });
       let link = this.baseurl+'sales/salesapi/ws/v1/leads/removecompetitor';
	  
       let data = new FormData();
	   data.append("iCompetitorId", cid);
		
	   return   this.http.post(link, data,reqOptions).map(this.extractData).catch(this.handleError);
  } 
  

  deletecontact(ctid): Observable<any> {
	let headers = new Headers();

	headers.append('Vauthtoken','Bearer '+this.tokan);
	let reqOptions = new RequestOptions({ headers: headers });
	let link = this.baseurl+'sales/salesapi/ws/v1/leads/removecontact';
   
	let data = new FormData();
	data.append("iContactId", ctid);
	 
	return   this.http.post(link, data,reqOptions).map(this.extractData).catch(this.handleError);
} 


profile(profileobj): Observable<any> {
	let headers = new Headers();

	headers.append('Vauthtoken','Bearer '+this.tokan);
	let reqOptions = new RequestOptions({ headers: headers });
	let link = this.baseurl+'sales/salesapi/ws/v1/engineer/edit';
   
	let data = new FormData();
	data.append("first_name", profileobj.firstname);
	data.append("last_name", profileobj.lastname);
	data.append("phone", profileobj.phone);
	 
	return   this.http.post(link, data,reqOptions).map(this.extractData).catch(this.handleError);
} 

   


  addCustomer(customerObj,sid,cid): Observable<any> {
	   let headers = new Headers();
  //     headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
	   headers.append('Vauthtoken','Bearer '+this.tokan);
       let reqOptions = new RequestOptions({ headers: headers });
       let link = this.baseurl+'sales/salesapi/ws/v1/leads/addcustomer';
	  
       let data = new FormData();
	  
	    data.append("company", customerObj.comapnyname);
		data.append("contact_person", customerObj.firstname);
		
		if(customerObj.comapnyref){
		data.append("vCompanyRefName", customerObj.comapnyref);
		}
		data.append("iCompanyType", customerObj.comapnytype);
		
		data.append("iCompanyStatus", customerObj.comapnystatus);
		data.append("iIndustryType", customerObj.industrytype);
		
		if(customerObj.email){
		 data.append("email",customerObj.email);
		}
		
		if(cid){
	 	 data.append("iCountryId", cid);
		}
		
		if(sid){
		 data.append("state", sid);
		}
		
		if(customerObj.city){
		 data.append("city", customerObj.city);
			}
		if(customerObj.address){
		  data.append("address",customerObj.address);
		}
		if(customerObj.exportstate){
		  data.append("iExportState", customerObj.exportstate);
		 
		}
		if(customerObj.exportcity){
		 data.append("vExportCity", customerObj.exportcity);
		}
		
		if(customerObj.postalcode){
			data.append("zipcode", customerObj.postalcode);
		}
		if(customerObj.primaryphone){
		 data.append("tel", customerObj.primaryphone);
		}
		if(customerObj.secondaryphone){
		 data.append("vSecondaryPhone",customerObj.secondaryphone);
		}
		if(customerObj.website){
			data.append("vWebsite",customerObj.website);
		 }
		
        return   this.http.post(link, data,reqOptions).map(this.extractData).catch(this.handleError);
  }  
  
  
  addProduct(productObj,lid,tval,mid,cp1,cp2): Observable<any> {
	   let headers = new Headers();
     //  headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
	   headers.append('Vauthtoken','Bearer '+this.tokan);
       let reqOptions = new RequestOptions({ headers: headers });
       let link = this.baseurl+'sales/salesapi/ws/v1/leads/addproduct';
	  
       let data = new FormData();
	    data.append("lead_id", lid);
	    data.append("model_id", mid);
		
		if(productObj.controller){
		data.append("controller_id", productObj.controller);
		}
		
		if(productObj.machineval){
		data.append("value_per_machine", productObj.machineval);
		  }
	   if(productObj.qty){
		data.append("quantity", productObj.qty);
		}
	    if(productObj.tval){
		data.append("total_value",tval);
		}
		
		if(productObj.probability){
		data.append("poc_id",productObj.probability);
		}
		
		if(productObj.probabilityjyoti){
		data.append("iJyotiPur",productObj.probabilityjyoti);
		}
		
		if(productObj.quotationno){
		data.append("q_number", productObj.quotationno);
		}
		if(productObj.makingdate){ 
		data.append("dm_within", productObj.makingdate);
		}
		if(productObj.ordersatus){ 
		data.append("order_status", productObj.ordersatus);
		}
		
		if(productObj.orderfinaldate){ 
		data.append("order_final_date",productObj.orderfinaldate);
		}
		
	
		  
		if(productObj.finalmachinqty){  
		  data.append("final_machine_quantity", productObj.finalmachinqty);
		} 
		  
		if(productObj.finalval){
		data.append("final_value", productObj.finalval);
		}
		if(productObj.advanceamt){
		data.append("final_amount_value", productObj.advanceamt);
		}
		if(productObj.collectiondate){
		data.append("date_of_advance_collection",productObj.collectiondate);
		}
		
		
		if(productObj.paymentmode){
		  data.append("mode_of_payment", productObj.paymentmode);
		}
		
		if(productObj.currencytype){
		  data.append("currency", productObj.currencytype);
		}
		
		if(productObj.supporthd){
		data.append("support_for_ho[]",productObj.supporthd);
		}
		
		if(productObj.bankname){
		data.append("bank_name",productObj.bankname);
		}
		
		if(productObj.chequeno){
	    	data.append("cdtr_no", productObj.chequeno);
		}
		if(productObj.paymentterm){
		   data.append("payment_terms", productObj.paymentterm);
        }		
		
		if(productObj.reason){
		data.append("reason",productObj.reason);
		}
	   if(productObj.lostordrdue){
		data.append("lost_order_due_to",productObj.lostordrdue);
	   }
		 if(cp1){
		  data.append("competitor_1", cp1);
		 }
	  if(cp2){
		data.append("competitor_2", cp2);
	   }
		 if(productObj.othermodel){
	    	data.append("other_model_id",productObj.othermodel);
		 }
	//	data.append("attachment",productObj.photo);
	
	   if(productObj.comment){
		  data.append("comment",productObj.comment);
	   }

	   if(productObj.dLiftingDate){
		data.append("dLiftingDate",productObj.dLiftingDate);
	 }

	   
		
	
				
        return   this.http.post(link, data,reqOptions).map(this.extractData).catch(this.handleError);
  }  
  
  
  
  
   editProduct(productObj,pid,tval,mid,cp1,cp2): Observable<any> {
	   let headers = new Headers();
     //  headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
	   headers.append('Vauthtoken','Bearer '+this.tokan);
       let reqOptions = new RequestOptions({ headers: headers });
       let link = this.baseurl+'sales/salesapi/ws/v1/leads/editproduct';
	 
       let data = new FormData();
	    data.append("id", pid);
	    data.append("lead_id", productObj.lead_id);
	    data.append("model_id", mid);
		if(productObj.controller){
		data.append("controller_id", productObj.controller);
		}
		
		if(productObj.machineval){
		data.append("value_per_machine", productObj.machineval);
		  }
	   if(productObj.qty){
		data.append("quantity", productObj.qty);
		}
	    if(productObj.tval){
		data.append("total_value",tval);
		}
		
		if(productObj.probability){
		data.append("poc_id",productObj.probability);
		}
		if(productObj.probabilityjyoti){
			data.append("iJyotiPur",productObj.probabilityjyoti);
		}

		if(productObj.quotationno){
		data.append("q_number", productObj.quotationno);
		}
		if(productObj.makingdate){ 
		data.append("dm_within", productObj.makingdate);
		}
		if(productObj.ordersatus){ 
		data.append("order_status", productObj.ordersatus);
		}
		
		if(productObj.orderfinaldate){ 
		data.append("order_final_date",productObj.orderfinaldate);
		}
		
		
		  
		if(productObj.finalmachinqty){  
		  data.append("final_machine_quantity", productObj.finalmachinqty);
		} 
		  
		if(productObj.finalval){
		data.append("final_value", productObj.finalval);
		}
		if(productObj.advanceamt){
		data.append("final_amount_value", productObj.advanceamt);
		}
		if(productObj.collectiondate){
		data.append("date_of_advance_collection",productObj.collectiondate);
		}
	
		
		if(productObj.paymentmode){
		  data.append("mode_of_payment", productObj.paymentmode);
		}
		
		if(productObj.currencytype){
		  data.append("currency", productObj.currencytype);
		}
		
		if(productObj.supporthd){
		data.append("support_for_ho[]",productObj.supporthd);
		}
		
		if(productObj.bankname){
		data.append("bank_name",productObj.bankname);
		}
		
		if(productObj.chequeno){
	    	data.append("cdtr_no", productObj.chequeno);
		}
		if(productObj.paymentterm){
		   data.append("payment_terms", productObj.paymentterm);
        }		
		
		if(productObj.reason){
		data.append("reason",productObj.reason);
		}
	   if(productObj.lostordrdue){
		data.append("lost_order_due_to",productObj.lostordrdue);
	   }
		 if(cp1){
		  data.append("competitor_1", cp1);
		 }
	  if(cp2){
		data.append("competitor_2",cp2);
	   }
		 if(productObj.othermodel){
	    	data.append("other_model_id",productObj.othermodel);
		 }
	//	data.append("attachment",productObj.photo);
	
	   if(productObj.comment){
		  data.append("comment",productObj.comment);
	   }

	   if(productObj.dLiftingDate){
	    	data.append("dLiftingDate",productObj.dLiftingDate);
	    }
	   
	
				
        return   this.http.post(link, data,reqOptions).map(this.extractData).catch(this.handleError);
  }  
  
  
  
  
  
  
  
  
    addLead(leadObj,cid,cp1,cp2,cp3,cp4,cp5): Observable<any> {
	   let headers = new Headers();
  
	   headers.append('Vauthtoken','Bearer '+this.tokan);
       let reqOptions = new RequestOptions({ headers: headers });
       let link = this.baseurl+'sales/salesapi/ws/v1/leads/addlead';
	  let  itotal = "0";
	   let  icontacttotal = "1";
       let data = new FormData();

	    data.append("iCustId", cid);
		data.append("vFullName_1", leadObj.fullname_1);
		data.append("vEmail_1", leadObj.email_1);
		data.append("vContact_1", leadObj.contact_1);
		
		if(leadObj.fullname_2){
			data.append("vFullName_2", leadObj.fullname_2);
		    data.append("vEmail_2", leadObj.email_2);
		    data.append("vContact_2", leadObj.contact_2);
			icontacttotal = "2";
		}
		
		if(leadObj.fullname_3){
			data.append("vFullName_3", leadObj.fullname_3);
		    data.append("vEmail_3", leadObj.email_3);
		    data.append("vContact_3", leadObj.contact_3);
			icontacttotal = "3";
		}
		
				
		if(leadObj.fullname_4){
			data.append("vFullName_4", leadObj.fullname_4);
		    data.append("vEmail_4", leadObj.email_4);
		    data.append("vContact_4", leadObj.contact_4);
			icontacttotal = "4";
		}
		
		if(leadObj.fullname_5){
			data.append("vFullName_5", leadObj.fullname_5);
		    data.append("vEmail_5", leadObj.email_5);
		    data.append("vContact_5", leadObj.contact_5);
			icontacttotal = "5";
		}
		if(icontacttotal!='0'){
		  data.append("iTotalContact", icontacttotal);
		}
		
		
		data.append("iCustType", leadObj.customertype);
		
		data.append("iLeadSourchId", leadObj.leadsource);
		
		if(leadObj.secondaryphone){
			data.append("vSecondPhone", leadObj.secondaryphone);
		}
		if(leadObj.website){
	    	data.append("vWebSite",leadObj.website);
		}
		if(leadObj.remarks){
		   data.append("vRemark", leadObj.remarks);
		}
		if(leadObj.nextvisit){
			data.append("dNextVisitDate", leadObj.nextvisit);
		 }
		
		if(cp1){
		  data.append("vCompetitor_1", cp1);
		  data.append("vMachineType_1", leadObj.machinetype_1);
		  data.append("iQuantity_1", leadObj.quantity_1);
		    itotal = "1";
		}
		if(cp2){
		  data.append("vCompetitor_2", cp2);
		  data.append("vMachineType_2", leadObj.machinetype_2);
		  data.append("iQuantity_2", leadObj.quantity_2);
		    itotal = "2";
		}
		if(cp3){
		  data.append("vCompetitor_3", cp3);
		  data.append("vMachineType_3", leadObj.machinetype_3);
		  data.append("iQuantity_3", leadObj.quantity_3);
		    itotal = "3";
		}
		if(cp4){
		  data.append("vCompetitor_4", cp4);
		  data.append("vMachineType_4", leadObj.machinetype_4);
		  data.append("iQuantity_4", leadObj.quantity_4);
		    itotal = "4";
		}
		if(cp5){
		  data.append("vCompetitor_5", cp5);
		  data.append("vMachineType_5", leadObj.machinetype_5);
		  data.append("iQuantity_5", leadObj.quantity_5);
		    itotal = "5";
		}
		if(itotal!='0'){
		  data.append("iTotalCompetior", itotal);
		}
				
        return   this.http.post(link, data,reqOptions).map(this.extractData).catch(this.handleError);
  } 


  editLead(leadObj,editleadform,cid,lid,ncn,nct,cp1,cp2,cp3,cp4,cp5): Observable<any> { 
	   let headers = new Headers();
  
	   headers.append('Vauthtoken','Bearer '+this.tokan);
       let reqOptions = new RequestOptions({ headers: headers });
       let link = this.baseurl+'sales/salesapi/ws/v1/leads/editlead';
	  let  itotal = "0";
	  let  icontacttotal = "0";
       let data = new FormData();
      
	   data.append("iLeadId", lid);
	    data.append("iCustId", cid);
	
		data.append("iCustType", leadObj.customertype);
		
		data.append("iLeadSourchId", leadObj.leadsource);
		
		
		if(leadObj.secondaryphone){
			data.append("vSecondPhone", leadObj.secondaryphone);
		}
		if(leadObj.website){
	    	data.append("vWebSite",leadObj.website);
		}
		if(leadObj.remarks){
		   data.append("vRemark", leadObj.remarks);
		}
		if(leadObj.nextvisit){
			data.append("dNextVisitDate", leadObj.nextvisit);
		 }
		

		if(leadObj.fullname_1){
			if(nct>=1){
				data.append("iContactId_1", editleadform.contactid_1);
			}else{
				data.append("iContactId_1", "0");
			}
			
		  data.append("vFullName_1", leadObj.fullname_1);
		  data.append("vEmail_1", leadObj.email_1);
		  data.append("vContact_1", leadObj.contact_1);
		  icontacttotal = "1";
		}

		if(leadObj.fullname_2){
			if(nct>=2){
				data.append("iContactId_2", editleadform.contactid_2);
			}else{
				data.append("iContactId_2", "0");
			}
			
		  data.append("vFullName_2", leadObj.fullname_2);
		  data.append("vEmail_2", leadObj.email_2);
		  data.append("vContact_2", leadObj.contact_2);
		  icontacttotal = "2";
		}

		if(leadObj.fullname_3){
			if(nct>=3){
				data.append("iContactId_3", editleadform.contactid_3);
			}else{
				data.append("iContactId_3", "0");
			}
			
		  data.append("vFullName_3", leadObj.fullname_3);
		  data.append("vEmail_3", leadObj.email_3);
		  data.append("vContact_3", leadObj.contact_3);
		  icontacttotal = "3";
		}

		if(leadObj.fullname_4){
			if(nct>=4){
				data.append("iContactId_4", editleadform.contactid_4);
			}else{
				data.append("iContactId_4", "0");
			}
			
		  data.append("vFullName_4", leadObj.fullname_4);
		  data.append("vEmail_4", leadObj.email_4);
		  data.append("vContact_4", leadObj.contact_4);
		  icontacttotal = "4";
		}


		if(leadObj.fullname_5){
			if(nct>=5){
				data.append("iContactId_5", editleadform.contactid_5);
			}else{
				data.append("iContactId_5", "0");
			}
			
		  data.append("vFullName_5", leadObj.fullname_5);
		  data.append("vEmail_5", leadObj.email_5);
		  data.append("vContact_5", leadObj.contact_5);
		  icontacttotal = "5";
		}


		
		if(leadObj.competitor_1){
			if(ncn>=1){
				data.append("iCompetitorId_1", editleadform.competitorid_1);
			}else{
				data.append("iCompetitorId_1", "0");
			}
			
		  data.append("vCompetitor_1", cp1);
		  data.append("vMachineType_1", leadObj.machinetype_1);
		  data.append("iQuantity_1", leadObj.quantity_1);
		    itotal = "1";
		}
		if(leadObj.competitor_2){
			if(ncn>=2){
				data.append("iCompetitorId_2", editleadform.competitorid_2);
			}else{
				data.append("iCompetitorId_2", "0");
			}
		  data.append("vCompetitor_2", cp2);
		  data.append("vMachineType_2", leadObj.machinetype_2);
		  data.append("iQuantity_2", leadObj.quantity_2);
		    itotal = "2";
		}
		if(leadObj.competitor_3){
			if(ncn>=3){
				data.append("iCompetitorId_3", editleadform.competitorid_3);
			}else{
				data.append("iCompetitorId_3", "0");
			}
			
		  data.append("vCompetitor_3", cp3);
		  data.append("vMachineType_3", leadObj.machinetype_3);
		  data.append("iQuantity_3", leadObj.quantity_3);
		    itotal = "3";
		}
		if(leadObj.competitor_4){
			
			if(ncn>=4){
				data.append("iCompetitorId_4", editleadform.competitorid_4);
			}else{
				data.append("iCompetitorId_4", "0");
			}
			
		  data.append("vCompetitor_4", cp4);
		  data.append("vMachineType_4", leadObj.machinetype_4);
		  data.append("iQuantity_4", leadObj.quantity_4);
		    itotal = "4";
		}
		if(leadObj.competitor_5){
			
			if(ncn>=5){
				data.append("iCompetitorId_5", editleadform.competitorid_5);
			}else{
				data.append("iCompetitorId_5", "0");
			}
			
		  data.append("vCompetitor_5", cp5);
		  data.append("vMachineType_5", leadObj.machinetype_5);
		  data.append("iQuantity_5", leadObj.quantity_5);
		    itotal = "5";
		}
		if(itotal!='0'){
		  data.append("iTotalCompetior", itotal);
		}

		if(icontacttotal!='0'){
			data.append("iTotalContact", icontacttotal);
		  }

		 
				
        return   this.http.post(link, data,reqOptions).map(this.extractData).catch(this.handleError);
  }  
  
  
  


 reportsend(reportObj): Observable<any> {
	   let headers = new Headers();
      headers.append('Content-Type', 'application/json');
     
       let link = 'http://jyoti.co.in/webservice/send-report-a-problem.php';
	    let data = new FormData();

	   if(reportObj.product){
		        data.append("yourname", reportObj.product);
                data.append("yourname", reportObj.name);
				data.append("companyname",reportObj.comapny );
				data.append("designation", reportObj.description);
				data.append("address", reportObj.address);
				data.append("contactnumber", reportObj.contact);
				data.append("youremail", reportObj.email);
				data.append("weburl", reportObj.website);
				data.append("message", reportObj.message);
	   }else{						   
		        data.append("yourname", reportObj.name);
				data.append("companyname",reportObj.comapny );
				data.append("designation", reportObj.description);
				data.append("address", reportObj.address);
				data.append("contactnumber", reportObj.contact);
				data.append("youremail", reportObj.email);
				data.append("weburl", reportObj.website);
				data.append("message", reportObj.message);
	   }
        
     return   this.http.post(link, data,headers).map(this.extractData).catch(this.handleError);
		
 
  }  

  
  
 
  private extractData(res: Response) { 
    let body = res.json();
	
    return body || { };
  }
  
  private extractPostData(res: Response) {
    let body = res;
	console.log(body);
	body = body.json();
	console.log(body);
	
    return body || { };
  }
  
  
  

  
  
  

  private handleError (error: Response | any) {
	  
	     let errMsg: string;
		 let errObj:any;
		  if (error instanceof Response) {
			const body = error.json() || '';
			//const err = body.error || JSON.stringify(body);
			//errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
            errObj = body;
			//errMsg = body.message.error;
			 
		  } else {
			errMsg = error.message ? error.message : error.toString();
		  }
		  
		
			  if(errObj){
			   return Observable.throw(errObj);
			 }else{
				 errObj ={};
				 errObj.message.error ="Server error";
				 Observable.throw(errObj);
				 }
         
  }
  
     handleError1(error: any) {
	 	  
        let errorMsg = error.message || 'Network Error ! Try again later.';
        return Observable.throw(errorMsg);
    }
  
  
  
  

}
